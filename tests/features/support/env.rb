require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
require_relative 'helper.rb'
require 'capybara'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require 'pry'
require 'fileutils'
require 'oci8'
require 'mail'
require 'date'
require 'pdf-reader'
require 'allure-cucumber'
require_relative '../pages/util.rb'
require_relative '../pages/email.rb'
require_relative '../support/DAO/SQLConnect.rb'
require_relative '../support/DAO/Queries.rb'



BROWSER = ENV['BROWSER']
AMBIENTE = ENV['AMBIENTE']
CONFIG = YAML.load_file(File.dirname(__FILE__) + "/ambientes/#{AMBIENTE}.yml")
BANCO = YAML.load_file(File.dirname(__FILE__) + "/DAO/config.yml")

World(Helper)
World(Capybara::DSL)
World(Capybara::RSpecMatchers)

util = Util.new
util.criar_pasta_log

# Capybara.register_driver :selenium do |app|
#     caps = Selenium::WebDriver::Remote::Capabilities.chrome('goog:chromeOptions' => { args: ['start-maximized']})
#     $driver = Capybara::Selenium::Driver.new(app, {:browser => :chrome, :desired_capabilities => caps})
# end

Capybara.register_driver :selenium do |app|
    if BROWSER.eql?('chrome')
        @caps = Selenium::WebDriver::Remote::Capabilities.chrome('goog:chromeOptions' => { args: ['start-maximized']})
    elsif BROWSER.eql?('chrome_headless')
        @caps = Selenium::WebDriver::Remote::Capabilities.chrome('goog:chromeOptions' => { args: ['--headless']})
    end

    Capybara::Selenium::Driver.new(app, {:browser => :chrome, :desired_capabilities => @caps})
end

Capybara.configure do |config|
    config.default_driver = :selenium
    config.app_host = CONFIG['url_padrao']
    config.default_max_wait_time = 10
end

AllureCucumber.configure do |config|
    config.results_directory = "/allure_reports"
    config.clean_results_directory = true
    config.logging_level = Logger::INFO
    config.logger = Logger.new($stdout, Logger::DEBUG)
    config.environment = "staging"
    config.environment_properties = {
    custom_attribute: "VEICULOS"
}

    config.categories = File.new("categories/my_custom_categories.json")
end