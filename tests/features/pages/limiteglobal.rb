require_relative 'Util.rb'

class LimiteGlobal < Util

    set_url 'https://hmg-api.omni.com.br/sms/messages'

    element :elementinputddd, :xpath, "//input[@id='ddd']"
    element :elementinputtelefone, :xpath, "//input[@id='fone']"
    element :elementbuttonpesquisar, :xpath, "//button[text()='Pesquisa']"
    element :elementbuttoncontinuar, :xpath, "//button[contains(text(),' Continuar')]"
    element :elementbuttonvamosla, :xpath, "//button[contains(text(),' Vamos lá')]"
    element :elementeuquerocrediback, :xpath, "//button[contains(text(),' Eu quero Crediback!')]"
    element :elementbuttonconcluir, :xpath, "//button[contains(text(),' Concluir')]"  
    
    def pesquisar_sms(ddd,telefone)
        elementinputddd.set ddd
        elementinputtelefone.set telefone
        elementbuttonpesquisar.click
    end
     
    def pegar_link_sms
        textosms = all(:xpath,"//tr")[1].all(:xpath, "//td")[3].text
        link = textosms.split(" ").last
        return link
    end

    def eu_quero_crediback
        elementeuquerocrediback.click
    end

    def clicar_em_continuar
        elementbuttoncontinuar.click
    end

    def inserir_cpf(cpf)
        find(:xpath, "//input[@inputmode='numeric']").set cpf
        clicar_em_continuar
    end

    def clicar_vamos_la
        elementbuttonvamosla.click
    end

    def selecionar_vencimento
        find(:xpath, "//div[5]/mat-card").click
        clicar_em_continuar
    end

    def concordar_termos_e_contrato
        find(:xpath, "//*[@id='mat-checkbox-1']/label/div").checkbox
        find(:xpath, "//*[@id='mat-checkbox-2']/label/div").checkbox
        elementbuttonconcluir.click
    end

    def pegar_codigo_sms

    end
   
end