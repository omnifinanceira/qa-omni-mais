require_relative 'geradorrandomico.rb'
require 'pry'

class NovaFicha < GeradorRandomico

    set_url ''

    element :elementcontinuar, :xpath, "//i[@class='fa fa-arrow-right mlBt']"
    element :elementconfirmar, :xpath, "//app-modal-message//button[.='Continuar']"
    element :campoCep, "input[placeholder='00000-000']"
    element :campoCepParaComparacao, "input[name='cep']"
    element :btnCadastrarFichaPesados, '.btn-full-orange', text:'Cadastrar Ficha Pesados Brasil'
    element :txtComprovanteRenda, '.select2-search__field'

    def initialize
        @sql = Queries.new
     
    end

    def selecionar_opcao(opcao)
        find("app-button-selector", text: opcao, match: :prefer_exact, wait:10).click
    end

    def clicar_em_confirmar
        elementconfirmar.click
    end

    def clicar_em_continuar
        elementcontinuar.click
    end

    def selecionar_loja(nome_loja='LUSA VEÍCULOS')
        select(nome_loja, from:'seller-store')
    end

    def selecionar_vendedor
        select('Não cadastrado', from:'seller-name')
    end

    def fechar_alerta_cpf_ficha_existente
        begin
            if find('.modal-info',).visible?
                find('.btn-full-orange', text: 'Iniciar nova ficha').click
            end
        rescue Exception => ex
         ex.message
        end
    end

    def preencher_dados_cliente(cpf,nascimento, extrato = false, telefone = gerar_telefone_celular)
        
        find('input[name="cpf-cliente"]').set(cpf)
        find('input[name="data-nascimento-cliente"]').set(nascimento)

        fechar_alerta_cpf_ficha_existente

        find("input[name='celular-cliente']").set('11993142061')
        find("input[name='cliente-renda-comprovada']").set('500000')
        
        if extrato == true
            txtComprovanteRenda.click
            find("li", text:'Extrato Bancário').click
        end
    end

    def clicar_adicionar_avalista(avalista)
        avalista_preenchido = false
        if avalista > 0
            find(:xpath, "//app-switch[@name='q1']").click
            preencher_avalista_cliente
            preencher_veiculo_avalista
            avalista_preenchido = true
        end
        return avalista_preenchido
    end

    def preencher_avalista_cliente
        find(:xpath, "//input[@formcontrolname='cpf']").set(gerar_cpf)
        find(:xpath, "//select[@formcontrolname='grauParentesco']").find(:xpath, "//option[text()='Irmã(o)']").select_option
        find(:xpath, "//input[@formcontrolname='dataNascimento']").set(gerar_data_nascimento)
        find(:xpath, "//input[@formcontrolname='celular']").set(gerar_telefone_celular)
        find(:xpath, "//input[@formcontrolname='renda']").set(gerar_numero(7000,9999).to_s + '00')
    end

    def clicar_adicionar_conjuge(conjuge)
        if conjuge
            find(:xpath, "//app-switch[@name='conjuge']").click
            preencher_conjuge
        end
    end

    def preencher_conjuge
        find(:xpath, "//input[@formcontrolname='nomeConjuge']").set(gerar_nome)
        find(:xpath, "//input[@formcontrolname='cpfConjuge']").set(gerar_cpf)
        find(:xpath, "//input[@formcontrolname='dataNascimentoConjuge']").set(gerar_data_nascimento)
        find(:xpath, "//input[@formcontrolname='telefoneConjuge']").set(gerar_telefone_celular)
        find(:xpath, "//select[@formcontrolname='classeProfConjuge']").find(:xpath, "//option[text()='AUTONOMO']").select_option
        find(:xpath, "//select[@formcontrolname='cargoConjuge']").find(:xpath, "//option[text()='MOTORISTA DE CARGA (EMPREGADO OU AUTONOMO), CAMINHONEIRO']").select_option
    end


    def preencher_veiculo_avalista
        find(:xpath, "//app-switch[@formcontrolname='isVeiculoEmMeuNome']").click
        find(:xpath, "//select[@formcontrolname='veiculoCategoria']").find(:xpath, "//option[text()='AUTOMOVEL']").select_option(wait: 10)
        find(:xpath, "//select[@formcontrolname='veiculoMarca']").find(:xpath, "//option[text()='FORD']").select_option(wait: 10)
        find(:xpath, "//select[@formcontrolname='veiculoAno']").find(:xpath, "//option[text()='1990']").select_option(wait: 10)
        find(:xpath, "//select[@formcontrolname='veiculoModelo']").find(:xpath, "//option[text()='DEL']").select_option(wait: 10)
        find(:xpath, "//select[@formcontrolname='veiculoVersao']").find(:xpath, "//option[text()='REY BELINA GHIA G']").select_option
        find(:xpath, "//input[@formcontrolname='veiculoPlaca']").set('KYH0793')
    end

    def preencher_detalhes_avalista(avalista_preenchido)
        if avalista_preenchido
            find(:xpath, "//input[@id='user-name']").set(gerar_nome)
            find(:xpath, "//input[@id='user-email']").set(gerar_email)
            find(:xpath, "//select[@name='estadoCivilAvalista']").find(:xpath, "//option[text() =' SOLTEIRO ']").select_option
            find(:xpath, "//select[@id='user-id']").find(:xpath, "//option[text()='Registro     Geral - RG']").select_option
            find(:xpath, "//input[@name='numeroDocumentoAvalista']").set(gerar_rg)
            find(:xpath, "//input[@name='orgaoDocumentoAvalista']").set('SSP')
            find(:xpath, "//input[@name='emissaoDocumentoAvalista']").set(gerar_data_rg)
            find(:xpath, "//input[@name='nomeMaeAvalistaText']").set(gerar_nome)
            find(:xpath, "//input[@name='nomePaiAvalista']").set(gerar_nome)
            find(:xpath, "//select[@id='user-citizenship']").find(:xpath, "//option[text()  ='BRASILEIRA']").select_option
            find(:xpath, "//select[@id='uf-nat-cli']").find(:xpath, "//option[text()='SP - São  Paulo']").select_option
            find(:xpath, "//select[@id='user-city-cli']").find(:xpath, "//option[text()='SAO    PAULO']").select_option
            find(:xpath, "//select[@id='user-occupation-class']").find(:xpath, "//option[text() ='AUTONOMO']").select_option
            find(:xpath, "//select[@id='user-occupation']").find(:xpath, "//option[text()   ='MOTORISTA DE CARGA (EMPREGADO OU AUTONOMO), CAMINHONEIRO']").select_option
            find(:xpath, "//input[@id='user-patrimony']").set(gerar_numero(2000,10000) + "00")
            find(:xpath, "//input[@name='foneCelularAvalista']").set(gerar_telefone_celular)
            find(:xpath, "//input[@name='foneResidencialCliente']").set(gerar_telefone)
            find(:xpath, "//input[@name='foneComercialCliente']").set(gerar_telefone)
            find(:xpath, "//input[@id='user-reference']").set(gerar_nome)
            find(:xpath, "//input[@id='user-reference-phone']").set(gerar_telefone_celular)
            find(:xpath, "//select[@id='user-reference-relation']").find(:xpath, "//option[text()='Amigo']").select_option
            clicar_em_continuar
            endereco_avalista
            clicar_em_continuar
        end
    end

    def endereco_avalista
        find(:xpath,"//input[@id='cep']").set('03440040')
        find(:xpath, "//input[@name='numero']").set('150')
        find(:xpath, "//select[@id='user-mail-address']").find(:xpath, "//option[text()='Residencial']").select_option
        
    end

    def adicionar_mais_avalista
        find(:xpath, "//app-switch[@formcontrolname='temProximo']").click
    end

    # def marcar_caminhao_proprio
    #     # caminhao_proprio_preenchido = false
    #     # if caminhao_proprio
    #         find(:xpath, "//app-switch[@ng-reflect-id='simPossuiCaminhaoEmSeuNome']").click
    #         find(:xpath, "//input[@formcontrolname='quantidadeCaminhao']").set(0)
    #     #     for i in 1..quantidade_caminhao_proprio-1
    #     #         find(:xpath, "//input[@formcontrolname='veiculoPlaca']").set(@sql.pegar_placa('pesado'))
    #     #         salvar_caminhao
    #     #         find(:xpath, "//a[text()=' Adicionar Veículo']").click
    #     #     end
    #     #     caminhao_proprio_preenchido = true
    #     # end
    #     # return caminhao_proprio_preenchido
    # end

    def salvar_caminhao
        find(:xpath, "//a[text()=' Salvar Caminhão ']").click
    end        

    def nao_sei_placa_caminhao_proprio
        find(:xpath, "//app-switch[@formcontrolname='isNaoSeiPlaca']").click
        find(:xpath, "//select[@formcontrolname='veiculoMarca']").find(:xpath, "//option[text()='FORD']").select_option
        find(:xpath, "//select[@formcontrolname='veiculoAno']").find(:xpath, "//option[text()='1990']").select_option
        find(:xpath, "//select[@formcontrolname='veiculoModelo']").find(:xpath, "//option[text()='F-11000']").select_option
        find(:xpath, "//select[@formcontrolname='veiculoVersao']").find(:xpath, "//option[text()='2P (DIESEL)']").select_option
        salvar_caminhao
    end

    def veiculo_zero
        find(:xpath, "//app-switch[@name='veiculoKM']").click
    end

    def preencher_dados_veiculo(placa)
        find('#veiculoPlaca').native.send_keys(placa)
        select('SP', from:'uf-licensing')
    end

    def adicionar_garantia(quantidade_garantia)
        unless quantidade_garantia == 0
            for i in 1..quantidade_garantia-1
                find(:xpath, "//a[text()=' Adicionar Veículo']").click
                find(:xpath, "//input[@id='veiculoPlaca']").native.send_keys(@sql.pegar_placa('pesado'))
                find(:xpath, "//select[@name='uf-licensing']").find(:xpath, "//option[text()='SP - São Paulo']").select_option
                find(:xpath, "//a[text()=' Salvar Veículo ']").click
            end
        end
    end

    def preencher_dados_veiculo_old(placa)
        find(:xpath, "//input[@id='placaVeiculo']").native.send_keys(placa)
        find(:xpath, "//select[@name='uf-licensing']").find(:xpath, "//option[text()='SP - São Paulo']").select_option
    end

    def verificar_img_load
        @horaatual = pegar_horario
        contagem = (Time.parse(@horaatual) + 120).strftime('%H:%M')
        begin
            while find("img[@alt='Carregando']").visible? && contagem != @horaatual
                @horaatual = pegar_horario
            end
            result = (contagem != @horaatual)
        rescue Exception => ex
            ex.message
        end
        return result
    end

    def verificar_pop_up_espera
        begin
            while find("img[@alt='Carregando']").visible? && contagem != @horaatual
                @horaatual = pegar_horario
            end
        rescue Exception => ex
            ex.message
        end
    end
   
    def alterar_retorno_seguro_assistencia
        find(:xpath, "//seguro/div/a/i").click
    end

    def pegar_numero_ficha
        $numero_proposta = current_url.split("/")[4]
    end

    def aguardar_retorno_tabela_financiamento
        begin
            while find(:xpath, "//div[@class='resultado-parcial__header']/span/span[@aria-hidden='true']").visible?
            end
        rescue
        end
    end
     
    def selecionar_valor_financiamento(percentual = 0.20)
        valorinicial = find('.max', wait: 60).text.gsub('R$ ', '').gsub('Máximo ','').to_f
        valorminimo = valorinicial*percentual
        find(".valor-financiado__action").click

        if valorminimo.round(4) < 2.0000
            find("input[formcontrolname='valorFinanciamento']").native.clear
            find("input[formcontrolname='valorFinanciamento']").set('200000')
        else
            find("input[formcontrolname='valorFinanciamento']").native.clear
            find("input[formcontrolname='valorFinanciamento']").set('1000000')
            # find(:xpath, "//input[@formcontrolname='valorFinanciamento']").set(valorminimo.round(4).to_s.split('.')[0].length == 1 ? valorminimo.round(4).to_s + '00' : valorminimo.round(4).to_s + '0')
        end
        click_button('Confirmar valor')
        aguardar_retorno_tabela_financiamento
        fechar_pop_up_parcelas
    end

    def fechar_pop_up_alerta_estrelas
        if page.has_css?('.modal-body')
            find(:xpath, '//div[@class="modal-footer"]/button[text()="Continuar"]').click
        end
    end

    def fechar_popup_alerta_parcelas
        if page.has_css?('.modal-header')
            find(:xpath, '//div[@class="modal-footer"]/button[text()="Ok"]').click
        end
    end

    def selecionar_valor_financiamento_alterado(percentual = 0.20)
        sleep(70)
        fechar_pop_up_parcelas
        valorinicial = find('.max', wait: 60).text.gsub('R$ ', '').gsub('Máximo ','').to_f
        valorminimo = valorinicial*percentual
        find(".valor-financiado__action").click

        if valorminimo.round(4) < 2.0000
            find("input[formcontrolname='valorFinanciamento']").native.clear
            find("input[formcontrolname='valorFinanciamento']").set('2.00000')
        else
            find("input[formcontrolname='valorFinanciamento']").native.clear
            find("input[formcontrolname='valorFinanciamento']").set(valorminimo.round(4).to_s + '00')
        end
        click_button('Confirmar valor')
        aguardar_retorno_tabela_financiamento
        fechar_pop_up_parcelas
    end

    def selecionar_valor_financiamento_old(percentual = 0.20)
        valorinicial = find(:xpath, "//p[@class='max']").text.gsub('R$ ', '').gsub('Máximo ','').to_f
        $valorminimo = valorinicial*percentual

        if $valorminimo.round(4) <= 2.000
            while find(:xpath, "//label[@class='valor-simular']").text.gsub('R$ ','').to_f >= 2.500
                find(:xpath, "//app-valor-financiado/div/div[1]/span[1]/a/i").click
            end
        else
            while find(:xpath, "//label[@class='valor-simular']").text.gsub('R$ ','').to_f > $valorminimo
                   find(:xpath, "//app-valor-financiado/div/div[1]/span[1]/a/i").click
            end
        end
        sleep(2)
    end

    def selecionar_parcelas(quantidade = '36')
        if verificar_mensagem_parcelamento_disponivel
         end
        @quantidade_parcela = quantidade
        begin
            find("label[for='#{@quantidade_parcela}']").select_option
        rescue Exception => ex
            ex.message
        end
    end

    def fechar_pop_up_parcelas
        begin
         sleep(10)
            if find('.modal-body').visible?
                click_button('Ok')
            end
        rescue Exception => ex
         ex.message
        end        
    end

    def verificar_mensagem_parcelamento_disponivel
        begin
            if find(:xpath, "//h4[contains(text(),'Algumas opções de parcelamento não disponíveis.')]").visible?
                find(:xpath, "(//button[@class='btn btn-full-orange pull-right' and contains(text(),'Ok')] )[12]").click
            end
            rescue Exception => ex
                ex.message
        end
    end

    def pegar_dados_financiamento_simulador
        parcelas_valor_auxiliar = find("label[for='#{@quantidade_parcela}']").text.split("\n")

        $quantidade_parcela_omni_mais = parcelas_valor_auxiliar[0].gsub('x','')
        $valor_parcela_omni_mais = parcelas_valor_auxiliar[1].gsub('R$ ','')
        puts "Simular - Proposta: " +  $numero_proposta + " Prazo: " + $quantidade_parcela_omni_mais + ' ' + "Valor: " + $valor_parcela_omni_mais
    end

    def preencher_detalhes_cliente_leves
        select('SP', from:'uf-nat-cli')
        select('SAO PAULO', from:'user-city-cli')
        select('SOLTEIRO', from:'estadoCivilCliente')
        select('BRASILEIRA', from:'user-citizenship')
        find("app-switch[name='ufn']").click
        find("input[name='foneCelularCliente']").set(gerar_telefone_celular)
        find("input[name='foneResidencialCliente']").set(gerar_telefone)
        find("input[name='foneComercialCliente']").set(gerar_telefone)
        find("input[name='nome']").set(gerar_nome)
        find("input[id='user-email']").set(gerar_email)
        find("input[name='numeroDocumentoCliente']").set(gerar_rg)
        find("input[name='orgaoDocumentoCliente']").set('SSP')
        find("input[name='emissaoDocumentoCliente']").set(gerar_data_rg)
        select('AUTONOMO', from:'user-occupation-class')
        select('MOTORISTA DE CARGA (EMPREGADO OU AUTONOMO), CAMINHONEIRO', from:'user-occupation')
        find("input[id='user-patrimony']").set(gerar_numero(2000,10000) + "00")
        find("input[formcontrolname='foneCelular']").set(gerar_telefone_celular)
        select('Amigo', from:'user-reference-relation')
        #binding.pry
       #find('#user-name').set('MARCO ANTONIO DE OLIVEIRA')

    end

    def preencher_detalhes_cliente(produto='')
        select('SP', from:'uf-nat-cli')
        select('SAO PAULO', from:'user-city-cli')
        select('SOLTEIRO', from:'estadoCivilCliente')
        select('BRASILEIRA', from:'user-citizenship')
        find("app-switch[name='ufn']").click
        find("input[name='foneCelularCliente']").set(gerar_telefone_celular)
        find("input[name='foneResidencialCliente']").set(gerar_telefone)
        find("input[name='foneComercialCliente']").set(gerar_telefone)
        find("input[name='nome']").set(gerar_nome)
        find("input[id='user-email']").set(gerar_email)
        find("input[name='numeroDocumentoCliente']").set(gerar_rg)
        find("input[name='orgaoDocumentoCliente']").set('SSP')
        find("input[name='emissaoDocumentoCliente']").set(gerar_data_rg)
        select('AUTONOMO', from:'user-occupation-class')
        select('MOTORISTA DE CARGA (EMPREGADO OU AUTONOMO), CAMINHONEIRO', from:'user-occupation')
        find("input[id='user-patrimony']").set(gerar_numero(2000,10000) + "00")
        find("input[formcontrolname='foneCelular']").set(gerar_telefone_celular)
        select('Amigo', from:'user-reference-relation')

        if(produto == 'pesados')
            select('mais de 10 anos', from:'tempoExperiencia')
            select('Cargas Perigosas', from:'tipoTransporte')
        end
    end

    def preencher_informacoes_caminhoes(preencher_informacoes_caminhoes,veiculo_quitado = false, implemento = false)
        find(:xpath, "//select[@name='tempo-caminhao']").find(:xpath, "//option[text()='de 5 à 10 anos']").select_option

        if veiculo_quitado
            find(:xpath, "//app-switch[@ng-reflect-id='simVeiculoQuitado']").click
        else
            find(:xpath, "//app-switch[@ng-reflect-id='naoVeiculoQuitado']").click
            find(:xpath, "//select[@name='banco']").find(:xpath, "//option[text()=' 341 - BANCO ITAU S/A ']").select_option
            find(:xpath, "//input[@formcontrolname='totalParcela']").set(gerar_numero(10,15))
            find(:xpath, "//input[@formcontrolname='valorParcela']").set(gerar_numero(300,900) + '00')
            find(:xpath, "//input[@formcontrolname='parcelasQuitada']").set(gerar_numero(1,9))
        end

        if implemento
            find(:xpath, "//app-switch[@ng-reflect-id='simVeiculoImplemento']").click
            find(:xpath, "//select[@formcontrolname='veiculoMarca']").find(:xpath, "//option[text()='BITREM']").select_option
            find(:xpath, "//select[@formcontrolname='veiculoAno']").find(:xpath, "//option[text()='2012']").select_option
            find(:xpath, "//select[@formcontrolname='veiculoModelo']").find(:xpath, "//option[text()='CARGA']").select_option
        else
            find(:xpath, "//app-switch[@ng-reflect-id='naoVeiculoImplemento']").click
        end
    end

    def endereco_cliente(old = false)
        if old
            find(:xpath,"//input[@id='cep']").set('03440040')
            find(:xpath, "//input[@name='numero']").set('66')
            find(:xpath, "//select[@id='user-mail-address']").find(:xpath, "//option[text()='Residencial']").select_option
        else
            find(:xpath,"//input[@id='cep']").set('01435001')
            find(:xpath, "//input[@name='numero']").set('66')
            find(:xpath, "//select[@id='condicoes-residencia']").find(:xpath, "//option[text()='FAMILIAR']").select_option
            find(:xpath, "//input[@id='residence-time']").set(gerar_data_tempo_residencia)
            find(:xpath, "//select[@id='user-mail-address']").find(:xpath, "//option[text()='Residencial']").select_option
        end
    end

    def observacoes
        find(:xpath, "//*[@id='user-observations']").set("Teste automação QA")
    end

    def pegar_dados_financiamento_finalizar
        parcelas_valor_auxiliar = all(:xpath, "//div[@class='panel costumer']")[2].text.split('(')[1].split(' ')
        puts "Finalizar - Proposta: " +  $numero_proposta + " Prazo: " + parcelas_valor_auxiliar[0].gsub('x','') + ' ' + "Valor: " + parcelas_valor_auxiliar[2].gsub('R$ ', '').gsub(')','')
    end

    def clicar_adicionar_avalista_observacoes
        find(:xpath, "//div/a[text()=' Incluir Avalista']").click
    end

    def preencher_avalista_observacoes
        find(:xpath, "//input[@id='avalista-cpf']").set(gerar_cpf)
        all(:xpath, "//select[@id='relacao']")[0].find(:xpath, "//option[text()='1 - Amigo']").select_option
        find(:xpath, "//input[@id='data-nascimento']").set(gerar_data_nascimento)
        find(:xpath, "//input[@id='celular-avalista']").set(gerar_telefone_celular)
        find(:xpath, "//input[@id='avalista-renda-comprovada']").set(gerar_numero(7000,9999).to_s + '00')
        find(:xpath, "//input[@id='nome-avalista']").set(gerar_nome)
        find(:xpath, "//input[@id='email-avalista']").set(gerar_email)
        all(:xpath, "//select[@id='relacao']")[1].find(:xpath, "//option[text()='Solteiro']").select_option
        find(:xpath, "//select[@id='tipo-identificacao']").find(:xpath, "//option[text()='RG']").select_option
        find(:xpath, "//input[@id='numero-documento']").set(gerar_rg)
        find(:xpath, "//input[@name='orgao-expedidor']").set('SSP')
        find(:xpath, "//input[@id='data-emissao']").set(gerar_data_rg)
        find(:xpath, "//input[@name='nome-mae']").set(gerar_nome)
        find(:xpath, "//input[@name='nome-pai']").set(gerar_nome)
        find(:xpath, "//select[@id='idNacionalidade']").find(:xpath, "//option[text()='BRASILEIRA']").select_option
        all(:xpath, "//select[@id='uf']")[0].all(:xpath, "//option[@value='SP']")[0].select_option
        all(:xpath, "//select[@id='cidade']")[0].find(:xpath, "//option[text()='SAO PAULO']").select_option
        find(:xpath,"//input[@id='cep']").set('03440040')
        find(:xpath, "//input[@name='numero']").set('66')
        all(:xpath, "//select[@id='user-occupation-class']")[0].find(:xpath, "//option[text()='AUTONOMO']").select_option
        find(:xpath, "//select[@id='user-occupation']").find(:xpath, "//option[text()='MOTORISTA DE CARGA (EMPREGADO OU AUTONOMO), CAMINHONEIRO']").select_option
        find(:xpath, "//input[@id='valor-patrimonio']").set(gerar_numero(20000,100000) + "00")
        find(:xpath, "//select[@id='tipo']").find(:xpath, "//option[text()='Residencial']").select_option
        find(:xpath, "//input[@id='nome-referencia']").set(gerar_nome)
        find(:xpath, "//input[@id='telefone-referencia']").set(gerar_telefone_celular)
    end

    def clicar_salvar
        find(:xpath, "//button[text()='Salvar ']").click
    end

    def verificar_img_load_analise
        @horaatual = pegar_horario
        contagem = (Time.parse(@horaatual) + 120).strftime('%H:%M')
        begin
            while find(:xpath, "//div[@class='loading-logo']").visible? && contagem != @horaatual
                @horaatual = pegar_horario
            end
            result = (contagem != @horaatual)
        rescue Exception => ex
            ex.message
        end
        return result
    end

    def gravar_numero_proposta
        find(:xpath,"//a[@class='navbar-brand']").click
        all(:xpath,"//a[@title='Ver ficha detalhada']")[0].click
        textoficha = find(:xpath, "//div[@class='col-xs-4 text-left']/span").text
        return textoficha
    end

    def acessar_proposta_aprovada_omnimais(numero_proposta)
        busca_proposta = @@numPropostaBuscar
        
        find(:xpath, "//a[@href='#aprovadas']", wait:30).click

        if find(:xpath, "//span[text()='#{busca_proposta}']//preceding::a[1]").visible?
            find(:xpath, "//span[text()='#{busca_proposta}']//preceding::a[1]").click
        else 
            scroll_until_findElement
        end
    end

    def scroll_until_findElement
        #while find(:xpath, "//span[text()='#{$numero_proposta}']//preceding::a[1]").visible?
        busca_propos = @@numPropostaBuscar
           execute_script("arguments[0].scrollIntoView(true)", find(:xpath, "//span[text()='#{busca_propos}']//preceding::a[1]"))
           find(:xpath, "//span[text()='#{busca_propos}']//preceding::a[1]").click
       # end
    end

    def captura_numero_proposta
        @@numPropostaBuscar = find(:xpath, "//span[@class='id-proposta-info']").text  

    end

    def emissao_pre_contrato
        find(:xpath, "//button[text()='SIM ']").click
        find(:xpath, "//button[text()=' Emitir Pré-Contrato ']").click
        find(:xpath, "//input[@id='1']").click
        all(:xpath, "//button[text()=' Emitir Pré-Contrato ']")[1].click
    end

    def anexo_documentos
        page.attach_file("#{Dir.pwd}\\imagens\\teste.jpg") do
            page.find(:xpath, "//div[text()=' TERMO DE ADESÃO - SEGURO PRESTAMISTA PREMIUM ']").click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\teste1.jpg") do
            page.find(:xpath, "//div[text()=' FICHA CADASTRAL ']").click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\teste2.jpg") do
            page.find(:xpath, "//div[text()=' CCB - CÉDULA DE CRÉDITO BANCÁRIO ']").click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\teste3.jpg") do
            page.find(:xpath, "//div[text()=' RG ']").click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\teste4.jpg") do
            page.find(:xpath, "//div[text()=' COMPROVANTE DE ENDEREÇO ']").click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\teste5.jpg") do
            page.find(:xpath, "//div[text()=' COMPROVANTE DE ENDEREÇO ']").click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\teste6.jpg") do
            page.find(:xpath, "//div[text()=' DECALQUE DO CHASSI ']").click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\teste7.jpg") do
            page.find(:xpath, "//div[text()=' CRV - CERTIFICADO DE REGISTRO DO VEÍCULO ']").click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\teste8.jpg") do
            page.all(:xpath, "//img[@alt='Foto garantia']")[0].click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\teste9.jpg") do
            page.all(:xpath, "//img[@alt='Foto garantia']")[1].click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\teste10.jpg") do
            page.all(:xpath, "//img[@alt='Foto garantia']")[2].click
        end

        find(:xpath, "//button[text()='Enviar Documentos']").click
    end

    def preenche_cep
        campoCep.set($cepValido)
    end
    
    def pesquisarCep
        click_button('Consultar CEP')
    end

    def encontrarBotaoCadastro
        if btnCadastrarFichaPesados.visible?
          return true
        else 
          return false
        end
    end

    def cadastrar_ficha_pesados 
        btnCadastrarFichaPesados.click
    end

    def comparaCep
        campoCep = campoCepParaComparacao.value.sub('-','')
        if campoCep.eql?($cepValido)
          return true
        else
          return false
        end
    end

    def preenche_info_veiculo(categoria, marca, ano, modelo, versao, cor, combustivel, estado)
        select(categoria, from:'categoria-veiculo')
        select(marca, from:'marca-veiculo')
        select(ano, from:'ano-veiculo')
        select(modelo, from:'vehicle-model')
        select(estado, from:'uf-licensing')
        select(versao, from:'versao-veiculo')
        select(cor, from:'cor-veiculo')
        select(combustivel, from:'veiculoCombustivel-veiculo')
        find('.btn-full-orange', text: 'Continuar').click
    end

    def verificar_img_load
        @horaatual = pegar_horario
        contagem = (Time.parse(@horaatual) + 120).strftime('%H:%M')
        begin
            while find(:xpath, "//img[@alt='Carregando']").visible? && contagem != @horaatual
                @horaatual = pegar_horario
            end
            result = (contagem != @horaatual)
        rescue Exception => ex
            ex.message
        end
        return result
    end

    def verificar_pop_up_espera
        begin
            while find(:xpath, "//img[@alt='Carregando']").visible? && contagem != @horaatual
                @horaatual = pegar_horario
            end
        rescue Exception => ex
            ex.message
        end
    end

    def verifica_observacoes
        if find(:xpath, '//h2[text()="Observações"]')
          return true
        else
          return false
        end
    end
    # def fechar_pop_up_parcelas
    #     begin
    #         if find(:xpath, "//app-ficha-cadastro-resultado-parcial/app-modal-message[2]/div/div/div/div[2]/div/div/h4").visible?
    #             find(:xpath, "//app-ficha-cadastro-resultado-parcial/app-modal-message[2]/div/div/div/div[3]/button[text()='Continuar']").click
    #         end
    #     rescue Exception => ex
    #      ex.message
    #     end        
    # end

end