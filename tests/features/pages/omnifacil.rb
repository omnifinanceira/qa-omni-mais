require_relative 'util.rb'
require 'pry'

class OmniFacil < Util

    set_url 'https://hml-omnifacil2.omni.com.br/hml/pck_login.prc_login'

    element :elementinputusuario, :xpath, "//input[@id='p-nome']"
    element :elementinputsenha, "input[id='p-senha']"
    element :elementconectar, :xpath, "//button[@id='btn-conectar']"
    element :elementhabilitarlistaagente, :xpath, "//span[@role='combobox']"
    element :elementselecionaragente, :xpath, "//li[text()='184 - BATISTELLA - MARINGA-PR']"
    element :elementvalidar, "button[id='bt-validar']"
    element :elemetmenu, :xpath, "//*[@id='navbar-collapse-1']/ul[1]/li[2]/ul/li[5]/ul/li[3]/a"
    element :elementfontordenarproposta, :xpath, "//font[@title='Ordenação crescente pelo Número da Proposta']"
    element :msgSemSeguro, ".txtVermelhoBold10"

    def logar_usuario(usuario = $usuarioagente,senha = $senhaagente)
        elementinputusuario.set usuario
        elementinputsenha.set senha
        elementconectar.click
    end

    def selecionar_menu_inicial(nome_menu)
        find(:xpath, "//*[text()='#{nome_menu}']", wait:30).click
    end

    def listar_agente
        elementhabilitarlistaagente.click
        elementselecionaragente.click
        elementvalidar.click
    end

    def selecionar_menu(menu = nil)
        if !menu.nil?
            find(:xpath, "//li/a[text()=' #{menu}']").click
        end
    end

    def selecionar_submenu(submenu = nil)
        if !submenu.nil?
            find(:xpath, "//ul/li/a[text()='#{submenu}']").click 
        end
    end

    def marcar_proposta_omni_mais
        within_frame(find(:xpath, "//iframe")[:id]) do
            within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[0][:name]) do
                find(:xpath, "//input[@id='chk-mostra']").click
            end
        end
    end

    def consultar_ficha(numero_proposta)
        
        #proposta em preenchimento
        @erro = ""
        within_frame(find(:xpath, "//iframe")[:id]) do
            within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
                begin
                    page.send_keys [:control, :end]
                    find(:xpath, "//input[contains(@onclick,'#{numero_proposta}')]").click
                rescue Exception => ex
                    @erro = ex.message
                end
            end
        end

        return @erro
    end

    # def acessar_pagamento(numero_proposta)
    #     sleep(5)
    #     within_frame(find(:xpath, "//iframe")[:id]) do
    #         find(:xpath, "//a[text()='#{numero_proposta}']").all(:xpath, "//input[@value='Liberado']")[21].click
    #     end
    # end

    def acessar_etapa(nome_etapa)
        within_frame(find("iframe")) do
            if page.has_css?('#popup_title')
                click_button('OK')
            end
            find(:xpath, "//input[@value='#{nome_etapa}']").click
        end
    end

    def consulta_historica_avalista
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[contains(@value, 'Avalista']").click
        end
    end

    def aceitar_etapa
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[contains(@onclick, 'APROVADA')]").click
            page.accept_alert
        end
    end
    
    def pegar_valor_prazo_omnifacil_alterado
        @dados_proposta_sucesso = []
        within_frame(find(:xpath, "//iframe")[:id]) do
            find('#P_PROP_VLR_LIBERADO').native.clear
            while !page.has_css?('#popup_title')
                sleep(1)
            end
            while page.has_css?('#popup_title')
                click_button('OK')
            end
            find('#P_PROP_VLR_LIBERADO').set('1480000')
            send_keys :tab
            find('#P_PROP_VLR_LIBERADO').click
            send_keys :tab
            sleep(10)
            @dados_proposta_sucesso << find(:xpath, "//input[@id='vl_prest']")[:value]
            @dados_proposta_sucesso << find(:xpath, "//input[@id='P_PROP_NUM_PREST']")[:value]
        end

        puts "OmniFacil - Proposta: " +  $numero_proposta + " Prazo: " + @dados_proposta_sucesso[1] + ' ' + "Valor: " + @dados_proposta_sucesso[0]
        return @dados_proposta_sucesso
    end

    def pegar_valor_prazo_omnifacil
        @dados_proposta_sucesso = []

        within_frame(find(:xpath, "//iframe")[:id]) do
            @dados_proposta_sucesso << find(:xpath, "//input[@id='vl_prest']")[:value]
            @dados_proposta_sucesso << find(:xpath, "//input[@id='P_PROP_NUM_PREST']")[:value]
        end

        puts "OmniFacil - Proposta: " +  $numero_proposta + " Prazo: " + @dados_proposta_sucesso[1] + ' ' + "Valor: " + @dados_proposta_sucesso[0]
        return @dados_proposta_sucesso
    end

    def clicar_gravar
        send_keys [:control, :end]
        within_frame(find(:xpath, "//iframe")[:id], wait:10) do
            while !find(:xpath, "//input[@value='Gravar']").visible?
            end
            find(:xpath, "//input[@value='Gravar']", wait:10).click
            page.accept_alert
        end
    end

    def enviar_mesa_omni
        within_frame(find(:xpath, "//iframe")[:id]) do
            if page.has_css?('#popup_title')
                click_button('OK')
            end
            find(:xpath, "//input[@value='Enviar Mesa Omni']").click
            page.accept_alert
        end
    end

    def preencher_checklist
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[contains(@value, 'teste')]").click
            all(:xpath, "//textarea[@class='texto']").each{|i| i.set("QA automação teste")}

            find(:xpath, "//input[@value='Gravar']").click

            accept_confirm("Deseja enviar a proposta #{$numero_proposta} para a Mesa Omni?")
            dismiss_confirm("Deseja incluir/alterar o checklist antes de enviar a proposta para a Mesa Omni?")
        end
    end

    def acessar_grupo_proposta_mesa(cod_mesa_proposta)
        mesa = cod_mesa_proposta[0][0] + "_" + cod_mesa_proposta[0][2]
        within_frame(find('iframe')) do
            @nova_janela = window_opened_by {
                find(:xpath, "//div[@id='fila#{mesa}']").find(:xpath, "//div[@id='infoEspera#{mesa}']/a").click
            }
        end
    end

    def pop_up_alerta_mesa
        begin
            within_frame(find("frame[name='bottomFrame']")) do
                sleep(1)
                while page.has_css?("#popup_title")
                    sleep(1)
                    click_button('OK')
                end
            end
            while page.has_css?("#popup_title")
                sleep(1)
                click_button('OK')
            end
        rescue Exception => ex
         ex.message
        end
    end

    def preencher_procedencia
        within_frame(find("frame[name='bottomFrame']")) do
            
            while page.has_css?("#popup_title")
                sleep(1)
                click_button('OK')
            end

            all(:xpath, "//input[@name='P_PROCEDE_RES']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_RES']").set('QA TESTE AUTOMAÇÃO')

            all(:xpath, "//input[@name='P_PROCEDE_COM']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_COM']").set('QA TESTE AUTOMAÇÃO')

            all(:xpath, "//input[@name='P_PROCEDE_FAM1']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_FAM1']").set('QA TESTE AUTOMAÇÃO')

            all(:xpath, "//input[@name='P_PROCEDE_FAM2']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_FAM2']").set('QA TESTE AUTOMAÇÃO')
        end
    end

    def localizar_acessar_proposta_mesa(numero_proposta)
        #find(:xpath, "//input[@type='search']").set(numero_proposta)
        find(:xpath, "//a[text()='#{numero_proposta}']").click
    end

    def pegar_valor_prazo_mesa
        @dados_proposta_sucesso = []

        within_frame(find("frame[name='bottomFrame']")) do
            @dados_proposta_sucesso << find(:xpath, "//input[@id='vl_prest']")[:value]
            @dados_proposta_sucesso << find(:xpath, "//input[@id='P_PROP_NUM_PREST']")[:value]
        end

        puts "Mesa - Proposta: " +  $numero_proposta + " Prazo: " + @dados_proposta_sucesso[1] + ' ' + "Valor: " + @dados_proposta_sucesso[0]
        return @dados_proposta_sucesso
    end

    def aprovar_proposta_mesa(numero_proposta)

        @texto_confirma_proposta_aprovada = ""

        within_window @nova_janela do
            localizar_acessar_proposta_mesa(numero_proposta)
            pop_up_alerta_mesa
            preencher_procedencia
            pop_up_alerta_mesa
            clicar_gravar_mesa
            pop_up_alerta_mesa
            pegar_valor_prazo_mesa
            aceitar_etapa_mesa
            pop_up_alerta_mesa
            preencher_checklist_mesa
            within_frame(find("frame[name='bottomFrame']")) do
                @texto_confirma_proposta_aprovada = find(:xpath, "//form/center/font[@class='txtAzulBold12']").text
            end
        end

        return  @texto_confirma_proposta_aprovada
    end

    def aprovar_proposta_com_pendencia(numero_proposta)
        within_window @nova_janela do
            localizar_acessar_proposta_mesa(numero_proposta)
            pop_up_alerta_mesa
            preencher_procedencia
            aceitar_etapa_verificacoes
            pop_up_alerta_mesa
            binding.pry
        end
    end

    def aceitar_etapa_verificacoes
        within_frame(find("frame[name='bottomFrame']")) do
            click_button('Aceitar')
            binding.pry
            page.accept_alert
        end
    end

    def clicar_gravar_mesa
        send_keys [:control, :end]
        within_frame(find("frame[name='bottomFrame']")) do
            while !find(:xpath, "//input[@value='Gravar']").visible?
            end
            find(:xpath, "//input[@value='Gravar']").click
            sleep(10)
            accept_confirm("Os dados da proposta #{$numero_proposta} foram atualizados com sucesso.")
        end
    end

    

    def aceitar_etapa_mesa
        send_keys [:control, :end]
        within_frame(find("frame[name='bottomFrame']"))  do

            while page.has_css?("#popup_title")
                click_button('OK')
            end
            find(:xpath, "//input[contains(@onclick, 'APROVADA')]").click
            page.accept_alert
        end
    end

    def preencher_checklist_mesa
        within_frame(find("frame[name='bottomFrame']"))  do
            all(:xpath, "//textarea[@class='texto']").each{|i| i.set("QA automação teste")}
            click_button('Gravar')
            page.accept_alert
            click_button('Aceitar')
            accept_confirm("Deseja finalizar a proposta com status APROVADA?")
            find('textarea[name="p_parecer"]').set('QA Teste Automacao')
            click_button('Validar')
            accept_confirm("Deseja finalizar a proposta?")
        end
    end

    def preencher_dados_pagamento
            within_frame(find(:xpath, "//iframe")[:id]) do
                find(:xpath, "//select[@name='P_FAVORECIDO']").all(:xpath, "//option[text()='TERCEIRO']")[0].select_option
                find(:xpath, "//input[@name='P_CPF_CNPJ']").set('92228410000102')
                find(:xpath, "//input[@name='P_VALOR_FAVOREC']").set('5000000')
                find(:xpath, "//select[@name='P_COMBUSTIVEL']").find(:xpath, "//option[text()='BI-COMBUSTÍVEL']").select_option
                find(:xpath, "//select[@name='P_UF_PLACA']").all(:xpath, "//option[text()='SP']")[1].select_option
                find(:xpath, "//input[@name='P_PROP_VENDEDOR']").set('Omni')
                find(:xpath, "//input[@name='P_PROP_CPF_VENDEDOR']").set('92228410000102')
                find(:xpath, "//input[@name='P_RENAVAM']").set('97589764876')
                find(:xpath, "//input[@name='P_USUARIO_LIBERA']").set('331INES')
                find(:xpath, "//input[@name='P_SENHA_LIBERA']").set('A7F2TAKM')
                find(:xpath, "//input[@value='Validar']").click
                accept_confirm("Deseja realmente efetivar a proposta?")
                find(:xpath, "//input[@value='Voltar']").click
            end
    end

    def selecionar_agente
        select('331 - S.I. - SERVICOS - ARARAQUARA-SP', from: 'p-agente')
        click_button('Validar')
    end

    def seleciona_ficha_verificacoes
        within_frame(find(:xpath, "//iframe")[:id]) do
            if page.has_css?('#popup_title')
                click_button('OK')
            end
            click_button('Ficha/Verificações')
        end
    end

    def abrir_seguros
        within_frame(find(:xpath, "//iframe")[:id]) do
            @seleciona_seguro = window_opened_by { find(:xpath, "//td[font[text()='Seguro Contrato']]/input[@type='button']").click }
        end
    end

    def verificar_seguros
        within_window @seleciona_seguro do
            if page.has_css?('.txtVermelhoBold10')
                $msgVerificaSeguro = msgSemSeguro.text
            else
                $msgVerificaSeguro = 'Existem seguros disponiveis'
            end
        end
    end
end
