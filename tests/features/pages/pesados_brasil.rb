require_relative 'Util.rb'
require 'pry'

class PesadosBrasil < Util
 
  set_url 'https://hmg-omni-mais-front.omni.com.br/login'

    element :elementinputusuario, :xpath, "//input[@id='user-login']"
    element :elementinputsenha, :xpath, "//input[@id='user-password']"
    element :elementspanentrar, :xpath, "//a/span[text()='Entrar']"
    element :elementsContinuar,  :xpath, "//a[contains(text(),'Continuar')]"
    
  
    def logar_usuario(usuario = $usuarioagente1,senha = $senhaagente1)
        elementinputusuario.set usuario
        elementinputsenha.set senha
        elementspanentrar.click
    end

    def carteira 
    find("span" , text:'2779').click 
    click_button('Confirmar')
    end

    def validarTexto
      find("p" , text:'verificar')
      find("h2", text: 'Pesados')
      sleep(3)
      find("input[placeholder='00000-000']").click
    end

    def campoCepInvalido
      find("input[placeholder='00000-000']").set('11111111')
      click_button('Consultar')
      find("span", text: 'Digite um CEP válido para realizar a consulta')
    end
  
    def campoCepValido
      find("input[placeholder='00000-000']").set('19915706')
      click_button('Consultar')
      sleep(3)
      find("p" , text:'PAMLYSSA')
      find("button", text: 'Cadastrar Ficha Pesados Brasil')
    end

    def cepSemAgente
      find("input[placeholder='00000-000']").set('98700000')
      click_button('Consultar')
      find("span", text: 'CEP indísponível! Não será possível atender um cliente dessa região.') 
    end

    def inicio 
      cep = '19915706' 
      campoCepValido()
      click_button('Cadastrar Ficha Pesados Brasil')
      find("app-button-selector", text: 'Financiamento', match: :prefer_exact, wait:15).click
      elementsContinuar.click
      select('Não cadastrado', from:'seller-name')
      elementsContinuar.click
      valida = find('input[placeholder="00000-000"]').value
      valida2 = valida.sub('-', '')
      valida2.eql?(cep)
    end 

    def cadastro
     inicio()
      find("input[placeholder='00000-000']").set('11111111')
      send_keys :tab
      find("span", text: 'Digite um CEP válido para realizar a consulta')
      find("input[placeholder='00000-000']").set('98700000')
      send_keys :tab
      find("h5", text: 'CEP indisponível')
    end
 #
    def endereco
      cep = '19915706' 
      valida = find('input[placeholder="00000-000"]').value
      valida2 = valida.sub('-', '')
      valida2.eql?(cep)
      find("input[name='numero']").set('471')
      select('PROPRIA', from:'condicoesResidencia')
      find("input[id='residence-time']").set('12041999')
      select('Residencial', from:'user-mail-address')
    end

    def preenche_info_cliente_pesados_brasil(cpf, nascimento)
      find(:xpath, "//input[@name='cpf-cliente']").set(cpf)
      find(:xpath, "//input[@name='data-nascimento-cliente']").set(nascimento)

      fechar_alerta_cpf_ficha_existente

      find(:xpath, "//input[@name='celular-cliente']").set('44222222222')
      find(:xpath, "//input[@name='cliente-renda-comprovada']").set('900000')
      
      
      
      find('.btn-full-orange', text: 'Continuar').click
    end

    def seleciona_comprovante_renda
      
    end

    def fechar_alerta_cpf_ficha_existente
      begin
          if find(:xpath, "//app-ficha-cadastro-cliente/app-modal/div/div/div").visible?
              #encontrar o botão 'Iniciar nova ficha'
             find(:xpath, "//app-ficha-cadastro-cliente/app-modal/div/div/div/div[2]/div/div/div/div/button[3]").click
          end
      rescue Exception => ex
       ex.message
      end
    end

    

    def verificar_img_load
      @horaatual = pegar_horario
      contagem = (Time.parse(@horaatual) + 120).strftime('%H:%M')
      begin
          while find(:xpath, "//img[@alt='Carregando']").visible? && contagem != @horaatual
              @horaatual = pegar_horario
          end
          result = (contagem != @horaatual)
      rescue Exception => ex
          ex.message
      end
      return result
  end

  def verificar_pop_up_espera
    begin
        while find(:xpath, "//img[@alt='Carregando']").visible? && contagem != @horaatual
            @horaatual = pegar_horario
        end
    rescue Exception => ex
        ex.message
    end
  end

  def fechar_pop_up
    begin
        if find(:xpath, "//app-ficha-cadastro-resultado-parcial/app-modal-message[2]/div/div/div/div[2]/div/div/h4").visible?
            find(:xpath, "//app-ficha-cadastro-resultado-parcial/app-modal-message[2]/div/div/div/div[3]/button[text()='Continuar']").click
        end
    rescue Exception => ex
     ex.message
    end        
end

  

  def clicar_em_continuar
    find('.btn-full-orange', text: 'Continuar').click
  end

  def digitar_valor_financiado
    find('.valor-financiado__action').click
    find('input[name="valorFinanciamento"]').set('1000000')
    find('.btn-full-orange', text: 'Confirmar valor').click
    sleep(10)
  end

  def preencher_detalhes_cliente
    select('SP', from:'uf-nat-cli')
    select('SAO PAULO', from:'user-city-cli')
    select('SOLTEIRO', from:'estadoCivilCliente')
    select('BRASILEIRA', from:'user-citizenship')
    find("app-switch[name='ufn']").click
    find("input[name='foneCelularCliente']").set(gerar_telefone_celular)
    find("input[name='foneResidencialCliente']").set(gerar_telefone)
    find("input[name='foneComercialCliente']").set(gerar_telefone)
    find("input[name='nome']").set(gerar_nome)
    find("input[id='user-email']").set(gerar_email)
    find("input[name='numeroDocumentoCliente']").set(gerar_rg)
    find("input[name='orgaoDocumentoCliente']").set('SSP')
    find("input[name='emissaoDocumentoCliente']").set(gerar_data_rg)
    select('AUTONOMO', from:'user-occupation-class')
    select('MOTORISTA DE CARGA (EMPREGADO OU AUTONOMO), CAMINHONEIRO', from:'user-occupation')
    find("input[id='user-patrimony']").set(gerar_numero(2000,10000) + "00")
    find("input[formcontrolname='foneCelular']").set(gerar_telefone_celular)
    select('Amigo', from:'user-reference-relation')
    binding.pry
end

def endereco_cliente
        
  find(:xpath,"//input[@id='cep']").set('03440040')
  find(:xpath, "//input[@name='numero']").set('66')
  find(:xpath, "//select[@id='user-mail-address']").find(:xpath, "//option[text()='Residencial']").select_option
  
end


def verifica_pop_up_envio_fila_agente
  if find('.btn-full-orange', text: 'Sim, confirmo o envio').visible?
    return true
  else
    return false
  end
end

def cancela_envio_proposta_pesados
  find('.btn-border-orange', text: 'Não, voltar').click
end


end 