require_relative 'geradorrandomico.rb'
require 'pry'

class MotoEletrica < GeradorRandomico

    element :elementcontinuar, :xpath, "//i[@class='fa fa-arrow-right mlBt']"
    element :tipoCombustivel, "select[name='veiculoCombustivel-veiculo']"
    element :placaVeiculo, "input[id='veiculoPlaca']"
    element :btnComissao, ".icon-seguro"
    element :sliderComissao, :xpath, "//div[div[h3[.='PRODUTOS']]]//slider-retorno//div[contains(@class,'min-slider-handle')]"
    
    def selecionar_opcao(opcao)
        if opcao == 'Financiamento' || opcao == 'Refinanciamento'
            sleep(5)
        end

        find(:xpath, "//app-button-selector/div/div[text()='#{opcao}']").click
    end

    def selecionar_loja(nome_loja='2 P VEICULOS')
        find(:xpath, "//select[@name='seller-store']").find(:xpath, "//option[text()='#{nome_loja}']").select_option
    end

    def selecionar_vendedor(nome_vendedor='Não cadastrado')
        find(:xpath, "//select[@name='seller-name']").find(:xpath, "//option[text()='#{nome_vendedor}']").select_option   
    end

    def clicar_em_continuar
        elementcontinuar.click
    end
    
    def fechar_alerta_cpf_ficha_existente
        begin
            if find(:xpath, "//app-ficha-cadastro-cliente/app-modal/div/div/div").visible?
                #encontrar o botão 'Iniciar nova ficha'
               find(:xpath, "//app-ficha-cadastro-cliente/app-modal/div/div/div/div[2]/div/div/div/div/button[3]").click
            end
        rescue Exception => ex
         ex.message
        end
    end

    

    def selecionar_categoria(categoria)
        find(:xpath, "//select[@name='categoria-veiculo']").find(:xpath, "//option[text()='#{categoria}']").select_option
    end

    def selecionar_ano_modelo(ano)
        find(:xpath, "//select[@name='ano-veiculo']").find(:xpath, "//option[text()='#{ano}']").select_option
    end

    def selecionar_versao(versao)
        find(:xpath, "//select[@name='versao-veiculo']").find(:xpath, "//option[text()='#{versao}']").select_option
    end
    
    def selecionar_cor(cor)
        find(:xpath, "//select[@name='cor-veiculo']").find(:xpath, "//option[text()='#{cor}']").select_option
    end

    def selecionar_tipo_combustivel(combustivel)
        find(:xpath, "//select[@name='veiculoCombustivel-veiculo']").find(:xpath, "//option[text()='#{combustivel}']").select_option
    end
    def selecionar_uf(estado)
        find(:xpath, "//select[@name='uf-licensing']").find(:xpath, "//option[text()='#{estado}']").select_option
    end
    
    def fechar_pop_up_alerta_estrelas
        begin
            if find(:xpath, "//app-ficha-cadastro-resultado-parcial/app-modal-message[2]/div/div/div/div[2]/div/div/h4").visible?
                find(:xpath, "//app-ficha-cadastro-resultado-parcial/app-modal-message[2]/div/div/div/div[3]/button[text()='Continuar']").click
            end
        rescue Exception => ex
         ex.message
        end
    end

    def preencher_detalhes_cliente
        select('SP', from:'uf-nat-cli')
        select('SAO PAULO', from:'user-city-cli')
        select('SOLTEIRO', from:'estadoCivilCliente')
        select('BRASILEIRA', from:'user-citizenship')
        find("app-switch[name='ufn']").click
        find("input[name='foneCelularCliente']").set(gerar_telefone_celular)
        find("input[name='foneResidencialCliente']").set(gerar_telefone)
        find("input[name='foneComercialCliente']").set(gerar_telefone)
        find("input[name='nome']").set(gerar_nome)
        find("input[id='user-email']").set(gerar_email)
        find("input[name='numeroDocumentoCliente']").set(gerar_rg)
        find("input[name='orgaoDocumentoCliente']").set('SSP')
        find("input[name='emissaoDocumentoCliente']").set(gerar_data_rg)
        select('AUTONOMO', from:'user-occupation-class')
        select('MOTORISTA DE CARGA (EMPREGADO OU AUTONOMO), CAMINHONEIRO', from:'user-occupation')
        find("input[id='user-patrimony']").set(gerar_numero(2000,10000) + "00")
        find("input[formcontrolname='foneCelular']").set(gerar_telefone_celular)
        select('Amigo', from:'user-reference-relation')
       # binding.pry
    end

    def endereco_cliente
        
        find(:xpath,"//input[@id='cep']").set('03440040')
        find(:xpath, "//input[@name='numero']").set('66')
        find(:xpath, "//select[@id='user-mail-address']").find(:xpath, "//option[text()='Residencial']").select_option
        
    end

    def observacoes
        find(:xpath, "//*[@id='user-observations']").set("Teste automação QA")
    end

    def verifica_tipo_combustivel
        tipoCombustivel.select('ELÉTRICO')
        page.has_select?(tipoCombustivel[:name], selected: 'ELÉTRICO')
    end

    def inserir_placa_invalida
        placaVeiculo.set('BBB0000')
    end

    def verifica_fipe_nao_encontrado
        sleep(70)
       while !find('.btn-full-orange', text: 'Ok')
       end
       find('.btn-full-orange', text: 'Ok').click
       binding.pry
    end

    def clica_comissao
        sleep(70)
        $valorParcelaInicio = find("label[for='48']").text
        btnComissao.click
    end

    def altera_comissao
        slide = all(".slider-tick")
        sleep(10)
        binding.pry
        sliderComissao.drag_to(slide[1])
        click_button("Salvar")
        sleep(10)
        $valorParcelaFim = find("label[for='48']").text
        sleep(5)
    end
end