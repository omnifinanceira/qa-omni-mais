require_relative 'Util.rb'
require 'pry'

class EfetiveMais < Util

  set_url 'https://hml-omnifacil2.omni.com.br/hml/pck_login.prc_login'


    element :elementinputusuario, :xpath, "//input[@id='p-nome']"
    element :elementinputsenha, :xpath, "//input[@id='p-senha']"
    element :elementspanentrar, :xpath, "//button[@id='btn-conectar']"
  
    # def logar_usuario(usuario = $usuarioagente1,senha = $senhaagente1)
    #     elementinputusuario.set usuario
    #     elementinputsenha.set senha
    #     elementspanentrar.click
    # end
    
    def logar_usuario1(usuario = $usuarioagente2,senha = $senhaagente2)
      elementinputusuario.set usuario
      elementinputsenha.set senha
      elementspanentrar.click
  end
  
    def acessar_conta
        find(:xpath, "//button[@id='btn-conectar']").click
    end

    def novo_menu
        find(:xpath, "//li/a[@id='sel-sistema[1]']").click
        find(:xpath, "//span[@id='select2-p-agente-container']").click
        find(:xpath, "//ul/li[contains(text(),'331')]").click
        find(:xpath, "//button[@id='bt-validar']").click
    end

    def novo_menu1
      find(:xpath, "//li/a[@id='sel-sistema[1]']").click
      find(:xpath, "//span[@id='select2-p-agente-container']").click
      find(:xpath, "//ul/li[contains(text(),'317')]").click
      find(:xpath, "//button[@id='bt-validar']").click
  end
   
    def selecionar_menu
      find(:xpath, "//li/a[contains(text(),' OPERACIONAL')]").click

      find(:xpath,"//li/a[contains(text(),'CRÉDITO')]").click
    end

    def tela_efetive
      find(:xpath,"//li/a[contains(text(),'EFETIVE ')]").click
      sleep(3)
    end 
 
    def produto_moto 
      within_frame(find(:xpath, "//iframe")) do
        find(:xpath, "//div/form/div/div/select[@name='produtos']").click
        find(:xpath, "//select/option[@value='MOTO']").click
        find(:xpath, "//div/form/div/div/select[@name='anoModelo']").click
        find(:xpath, "//select/option[@value='2015']").click
        find(:xpath, "//div/form/div/div/select[@name='uf']").click
        find(:xpath, "//select/option[@value='SP']").click 
        find(:xpath, "//div/form/div/div/select[@name='cidade']").click
        find(:xpath, "//select/option[@value='SAO PAULO']").click
        find(:xpath, "//div/form/div/div/select[@name='entrada']").click
        find(:xpath, "//div/form/div/div/select[@name='entrada']/option[2]").click
        find(:xpath, "//div/form/div/div/select[@name='renda']").click
        find(:xpath, "//div/form/div/div/select[@name='renda']/option[5]").click
        find(:xpath, "//button[@class='btn-filter']").click
        all(:xpath, "//tr/td[contains(text(),'MOTO')]").length.should > 0

        sleep(2)
      end
    end 


    def preencher_filtro_efetive_mais(produto,ano,estado,cidade,entrada = "até 25%", compRenda = "até 10%")
      within_frame(find(:xpath, "//iframe")) do
         select(produto, from:'produtos')
         select(ano, from:'anoModelo')
         select(estado, from:'uf')
         select(cidade, from:'cidade')
         select(entrada, from:'entrada')
         select(compRenda, from:'renda')
         click_button('Filtrar')
        end
    end

    def produto_moto1
      within_frame(find(:xpath, "//iframe")) do
      #  find(:xpath, "//div/form/div/div/select[@name='produtos']").click
      #  find(:xpath, "//select/option[@value='MOTO']").click
       select('Moto', from:'produtos')
        # find(:xpath, "//div/form/div/div/select[@name='anoModelo']").click
        # find(:xpath, "//select/option[@value='2019']").click
       select('2019', from:'anoModelo')
        # find(:xpath, "//div/form/div/div/select[@name='uf']").click
        # find(:xpath, "//select/option[@value='SP']").click
       select('SP', from:'uf')
        # find(:xpath, "//div/form/div/div/select[@name='cidade']").click
        # find(:xpath, "//select/option[@value='SAO PAULO']").click
        select('SAO PAULO', from:'cidade')
        # find(:xpath, "//div/form/div/div/select[@name='entrada']").click
        # find(:xpath, "//div/form/div/div/select[@name='entrada']/option[3]").click
        select('até 25%', from:'entrada')
        # find(:xpath, "//div/form/div/div/select[@name='renda']").click
        # find(:xpath, "//div/form/div/div/select[@name='renda']/option[2]").click
        select('até 10%', from:'renda')
        # find(:xpath, "//button[@class='btn-filter']").click
         click_button('Filtrar')
        all(:xpath, "//tr/td[contains(text(),'MOTO')]").length.should > 0
        sleep(2)
      end
    end  

    def produto_veiculo_leve
      within_frame(find(:xpath, "//iframe")) do
        find(:xpath, "//div/form/div/div/select[@name='produtos']").click
        find(:xpath, "//select/option[@value='VEÍCULO LEVE']").click
        find(:xpath, "//div/form/div/div/select[@name='anoModelo']").click
        find(:xpath, "//select/option[@value='2007']").click
        find(:xpath, "//div/form/div/div/select[@name='uf']").click
        find(:xpath, "//select/option[@value='SP']").click
        find(:xpath, "//div/form/div/div/select[@name='cidade']").click
        find(:xpath, "//select/option[@value='SAO PAULO']").click
        find(:xpath, "//div/form/div/div/select[@name='entrada']").click
        find(:xpath, "//div/form/div/div/select[@name='entrada']/option[4]").click
        find(:xpath, "//div/form/div/div/select[@name='renda']").click
        find(:xpath, "//div/form/div/div/select[@name='renda']/option[3]").click
        find(:xpath, "//button[@class='btn-filter']").click
        all(:xpath, "//tr/td[contains(text(),'LEVE')]").length.should > 0
        sleep(2)
      end
    end

    def produto_veiculo_leve1
      within_frame(find(:xpath, "//iframe")) do
        find(:xpath, "//div/form/div/div/select[@name='produtos']").click
        find(:xpath, "//select/option[@value='VEÍCULO LEVE']").click
        find(:xpath, "//div/form/div/div/select[@name='anoModelo']").click
        find(:xpath, "//select/option[@value='2007']").click
        find(:xpath, "//div/form/div/div/select[@name='uf']").click
        find(:xpath, "//select/option[@value='SP']").click
        find(:xpath, "//div/form/div/div/select[@name='cidade']").click
        find(:xpath, "//select/option[@value='SAO PAULO']").click
        find(:xpath, "//div/form/div/div/select[@name='entrada']").click
        find(:xpath, "//div/form/div/div/select[@name='entrada']/option[6]").click
        find(:xpath, "//div/form/div/div/select[@name='renda']").click
        find(:xpath, "//div/form/div/div/select[@name='renda']/option[6]").click
        find(:xpath, "//button[@class='btn-filter']").click
        all(:xpath, "//tr/td[contains(text(),'LEVE')]").length.should > 0
        sleep(2)
      end
    end

    def produto_veiculo_pesado
      within_frame(find(:xpath, "//iframe")) do
        find(:xpath, "//div/form/div/div/select[@name='produtos']").click
        find(:xpath, "//select/option[@value='VEÍCULO PESADO']").click
        find(:xpath, "//div/form/div/div/select[@name='anoModelo']").click
        find(:xpath, "//select/option[@value='2008']").click
        find(:xpath, "//div/form/div/div/select[@name='uf']").click
        find(:xpath, "//select/option[@value='SP']").click
        find(:xpath, "//div/form/div/div/select[@name='cidade']").click
        find(:xpath, "//select/option[@value='SAO PAULO']").click
        find(:xpath, "//div/form/div/div/select[@name='entrada']").click
        find(:xpath, "//div/form/div/div/select[@name='entrada']/option[5]").click
        find(:xpath, "//div/form/div/div/select[@name='renda']").click
        find(:xpath, "//div/form/div/div/select[@name='renda']/option[4]").click
        find(:xpath, "//button[@class='btn-filter']").click
        all(:xpath, "//tr/td[contains(text(),'PESADO')]").length.should > 0
        sleep(2)
      end
    end

    def produto_todos
      within_frame(find(:xpath, "//iframe")) do
        find(:xpath, "//div/form/div/div/select[@name='produtos']").click
        find(:xpath, "//button[@class='btn-filter']").click
        sleep(4)
      end
    end 

    def  btn_limpar
      within_frame(find(:xpath, "//iframe")) do
        find(:xpath, "//div/form/div/div/select[@name='produtos']").click
        find(:xpath, "//select/option[@value='VEÍCULO LEVE']").click
        find(:xpath, "//div/form/div/div/select[@name='anoModelo']").click
        find(:xpath, "//select/option[@value='2007']").click
        find(:xpath, "//div/form/div/div/select[@name='uf']").click
        find(:xpath, "//select/option[@value='SP']").click
        find(:xpath, "//div/form/div/div/select[@name='cidade']").click
        find(:xpath, "//select/option[@value='SAO PAULO']").click
        find(:xpath, "//div/form/div/div/select[@name='entrada']").click
        find(:xpath, "//div/form/div/div/select[@name='entrada']/option[6]").click
        find(:xpath, "//div/form/div/div/select[@name='renda']").click
        find(:xpath, "//div/form/div/div/select[@name='renda']/option[6]").click
        find(:xpath, "//button[@class='btn-clear']").click
        sleep(2)
      end
    end  

    def btn_pesquisar_letra
      within_frame(find(:xpath, "//iframe")) do
      find(:xpath, "//input[@placeholder='Pesquise na tabela']").click
      find(:xpath, "//input[@placeholder='Pesquise na tabela']").set("moto")
     end
    end

    def btn_pesquisar_numero
      within_frame(find(:xpath, "//iframe")) do
      find(:xpath, "//input[@placeholder='Pesquise na tabela']").click
      find(:xpath, "//input[@placeholder='Pesquise na tabela']").set("2015")
     sleep(3)
     end
    end
    
    def  valor_financiar
      within_frame(find(:xpath, "//iframe")) do
      find(:xpath, "//input[@name='de']").click
      find(:xpath, "//input[@name='de']").set("20")
      find(:xpath, "//input[@name='ate']").click
      find(:xpath, "//input[@name='ate']").set("20000000")
      find(:xpath, "//button[@class='btn-filter']").click
      sleep(4)
      end
    end  

    def score
      within_frame(find(:xpath, "//iframe")) do
      find(:xpath, "//select[@name='score']").click
      find(:xpath, "//option[@value='5']").click
      find(:xpath, "//button[@class='btn-filter']").click
      find(:xpath, "//select[@name='score']").click
      find(:xpath, "//option[@value='4']").click
      find(:xpath, "//button[@class='btn-filter']").click
      find(:xpath, "//select[@name='score']").click
      find(:xpath, "//option[@value='3']").click
      find(:xpath, "//button[@class='btn-filter']").click
      find(:xpath, "//select[@name='score']").click
      find(:xpath, "//option[@value='2']").click
      find(:xpath, "//button[@class='btn-filter']").click
      find(:xpath, "//select[@name='score']").click
      find(:xpath, "//option[@value='1']").click
      find(:xpath, "//button[@class='btn-filter']").click
      find(:xpath, "//select[@name='score']").click
      find(:xpath, "//select[@formcontrolname='score']/option[text()='Todos']").click
      find(:xpath, "//button[@class='btn-filter']").click
      sleep(4)
      end
    end 
    
    def agente
      within_frame(find(:xpath, "//iframe")) do
      find(:xpath, "//input[@placeholder='Pesquise na tabela']").click
      find(:xpath, "//input[@placeholder='Pesquise na tabela']").set("331")
      first('.btn-detalhes').click
      sleep(2)  
      binding.pry
      click_button('Capturar Proposta')
      sleep(2)
      click_button('Confirmar e transferir')
      sleep(2)
      find(:xpath, "//p[contains(text(),'Não permitido recuperar')]")
      click_button('Voltar')
     end
    end

    def capturaProposta
      within_frame(find(:xpath, "//iframe")) do
        sleep(3)
      find(:xpath, "//input[@placeholder='Pesquise na tabela']").click
      sleep(3)
      find(:xpath, "//input[@placeholder='Pesquise na tabela']").set("317")#mudar para 317
      first('.btn-detalhes').click
      sleep(2)  
      click_button('Capturar Proposta')
      sleep(2)
      click_button('Confirmar e transferir')
      sleep(2)
      find(:xpath, "//p[contains(text(),'A proposta foi transferida para o seu Agente com sucesso!')]")
      click_button('Voltar')
     end
    end 

end
