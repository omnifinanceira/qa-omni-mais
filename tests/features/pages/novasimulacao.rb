require_relative 'geradorrandomico.rb'

class NovaSimulacao < GeradorRandomico

    element :elementcontinuar, :xpath, "//i[@class='fa fa-arrow-right mlBt']"

    def initialize
        @sql = Queries.new
    end

    def selecionar_opcao(opcao)
        if opcao == 'Financiamento' || opcao == 'Refinanciamento'
            sleep(3)
        end

        find(:xpath, "//app-button-selector/div/div[text()='#{opcao}']").click
    end

    def clicar_em_continuar
        elementcontinuar.click
    end

    def selecionar_loja(nome_loja='LUSA VEÍCULOS')
        find(:xpath, "//select[@name='seller-store']").find(:xpath, "//option[text()='#{nome_loja}']").select_option
    end

    def selecionar_ano_modelo(ano='2015')
        find(:xpath, "//select[@name='anoModelo']").find(:xpath, "//option[text()='#{ano}']").select_option
    end

    def preencher_valor_veiculo(valor="5000000")
        find(:xpath, "//input[@name='valorVeiculo']").set(valor)
    end

    def preencher_valor_financiado(valor="3000000")
        find(:xpath, "//input[@name='valorFinanciado']").set(valor)
    end

    def verificar_img_load
        @horaatual = pegar_horario
        contagem = (Time.parse(@horaatual) + 120).strftime('%H:%M')
        begin
            while find(:xpath, "//img[@alt='Carregando']").visible? && contagem != @horaatual
                @horaatual = pegar_horario
            end
            result = (contagem != @horaatual)
        rescue Exception => ex
            ex.message
        end
        return result
    end

end
