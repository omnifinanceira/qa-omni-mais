Dado('que acesso o omnifacil com usuário de parametro de cobrança') do
  @omnifacil = OmniFacil.new
  @omnifacil.load
  @omnifacil.logar_usuario($usuarioParametro,$senhaParametro) 
end

Dado('que acesso o omnifacil com usuário sem permissão ao parametro de cobrança') do
  @omnifacil = OmniFacil.new
  @omnifacil.load
  @omnifacil.logar_usuario($usuarioSemParametro,$senhaSemParametro) 
end

Quando('acesso os parametrôs de cobrança') do  
  @omnifacil.selecionar_menu_inicial(" NOVO MENU")
  @omnifacil.selecionar_agente
  @omnifacil.selecionar_menu('OPERACIONAL')
  @omnifacil.selecionar_submenu('CANAIS DIGITAIS')
  @omnifacil.selecionar_submenu('Parâmetros de Cobrança')
end

Entao('a pagina parametrôs de cobrança é aberta') do
  within_frame(find(:xpath, "//iframe")[:id]) do
    expect(assert_text('Parâmetros de Cobrança - Canais Digitais')).to be(true)
  end  
end

Quando('acesso o novo menu') do
  find(:xpath, "/html/body/div[3]/div[2]/div/div/div/div/div[4]/button[2]").click
  @omnifacil.selecionar_menu_inicial(" NOVO MENU")
  @omnifacil.selecionar_agente
  @omnifacil.selecionar_menu('OPERACIONAL')
end

Entao('o sub menu Canais Digitais não é apresentado') do
  expect(assert_no_text('CANAIS DIGITAIS')).to be(true)
end

E('eu selecionar no filtro Campanha Pesados Brasil') do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[1]/select").click
    sleep(1)
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[1]/select/option[5]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[6]/button[2]").click
  end
end 

Entao("deve listar somente os parâmetros referente a campanha Pesados Brasil") do
  within_frame(find(:xpath, "//iframe")[:id]) do
    within(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[2]/div[2]/table/tbody") do
      expect(assert_text('PESADOS BRASIL')).to be(true)
      expect(assert_no_text('LEADS')).to be(true)
      expect(assert_no_text('TRUCUDÃO')).to be(true)
      expect(assert_no_text('CAMPANHA TESTE')).to be(true)
    end
  end
end

E("seleciono o filtro Distripuição por Proximidade") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[2]/div/div[1]/label/input").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[6]/button[2]").click
  end
end 

Entao("devera listar somente os parâmetros referente a distribuição Proximidade") do
  within_frame(find(:xpath, "//iframe")[:id]) do
    within(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[2]/div[2]/table/tbody") do
      expect(assert_text('Proximidade')).to be(true)
      expect(assert_no_text('Porcentagem')).to be(true)
      expect(assert_no_text('Por Agente')).to be(true)      
    end
  end
end

E("eu seleciono o filtro Situação pelo checkbox Inativo") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[5]/div/div[2]/label/input").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[6]/button[2]").click
  end
end 

Entao("deve lista somente os parâmetros Inativos") do
  within_frame(find(:xpath, "//iframe")[:id]) do
    within(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[2]/div[2]/table/tbody") do
      expect(assert_text('Inativo')).to be(true)
      expect(assert_no_text('Ativo')).to be(true)            
    end
  end
end

E("informar o início da vigência uma data maior que o fim da vigência") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[4]/div[1]/input").set('06052022')
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[4]/div[2]/input").set('06052015')
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[6]/button[2]").click
  end
end 

Entao("não deve realizar a pesquisa") do
  within_frame(find(:xpath, "//iframe")[:id]) do
    expect(assert_text('0 parâmetros encontrado(s)')).to be(true)    
  end
end

E("eu efetuo alguma edição de Campanha") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[2]/div[2]/table/tbody/tr[2]/td[6]/div/button[1]/div/span").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[1]/select").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[1]/select/option[4]").click
    sleep(5)
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[2]/div[2]/button").click
    sleep(20)
    #find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[6]/button[2]").click
  end
end 

Entao("a Campanha deve ter sido alterada") do
  within_frame(find(:xpath, "//iframe")[:id]) do
    expect(assert_text('Parâmetro de Cobrança salvo com sucesso!')).to be(true)
  end
end

E("eu efetuo alguma edição de Distribuição de parâmetro") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[2]/div[2]/table/tbody/tr[2]/td[6]/div/button[1]/div/span").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[3]/div/div[2]/input")
    sleep(5)
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[2]/div[2]/button").click
    sleep(30)
  end
end

Entao("a Distribuição de parâmetro deve ter sido alterada") do
  within_frame(find(:xpath, "//iframe")[:id]) do
    expect(assert_text('Parâmetro de Cobrança salvo com sucesso!')).to be(true)
  end
end

E("eu acesso a pagina de edição") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[2]/div[2]/table/tbody/tr[2]/td[6]/div/button[1]/div/span").click
  end
end

E("e clico no botão voltar") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[2]/div[1]/button").click
  end
end

E("adiciono uma nova campanha truncadão") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[1]/button[3]/div").click
    sleep(2)
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[1]/select").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[1]/select/option[3]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[2]/div/div/input[1]").set("01102000")
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[2]/div/div/input[2]").set("13102023")
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/div[2]/app-custom-dual-list/div/div[1]/div/ul/li[1]/label").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/div[2]/app-custom-dual-list/div/div[2]/button[1]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[4]/div/div[1]/div/input").set('100')
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[4]/div/div[2]/select").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[4]/div/div[2]/select/option[1]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[2]/div[2]/button").click
  end
end

Entao("novo parametro é criado") do
  within_frame(find(:xpath, "//iframe")[:id]) do
    expect(assert_text('Parâmetro de Cobrança salvo com sucesso!')).to be(true)
    find(:xpath, "/html/body/modal-container/div/div/app-parametros-cobranca-modal-confirmacao/div[3]/button").click
    sleep(5)
  end
end

Quando("excluo um parametro de cobrança truncadão") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[1]/select").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[1]/select/option[4]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[6]/button[2]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[2]/div[2]/table/tbody/tr[1]/td[6]/div/button[3]/div/span").click
    find(:xpath, "/html/body/modal-container/div/div/app-parametros-cobranca-modal-atencao/div[3]/button[2]").click
  end
end

Entao("o parametro devera ser excluido") do
  sleep(5)
  within_frame(find(:xpath, "//iframe")[:id]) do
    expect(assert_text('O parâmetro foi excluído com sucesso!')).to be(true)
  end
end

E("adiciono uma nova campanha Pesados Brasil") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[1]/button[3]/div").click
    sleep(2)
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[1]/select").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[1]/select/option[4]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[2]/div/div/input[1]").set("01102000")
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[2]/div/div/input[2]").set("13102023")
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/div[2]/app-custom-dual-list/div/div[1]/div/ul/li[1]/label").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/div[2]/app-custom-dual-list/div/div[2]/button[1]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[4]/div/div[1]/div/input").set('100')
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[4]/div/div[2]/select").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[1]/form/div/div/div[4]/div/div[2]/select/option[1]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-novo-parametro/div/div[2]/div[2]/button").click
  end
end

Quando("excluo um parametro de cobrança Pesados Brasil") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[1]/select").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[1]/select/option[5]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/app-parametros-cobranca-filtro/div/accordion/accordion-group/div/div[2]/div/form/div/div[6]/button[2]").click
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[2]/div[2]/table/tbody/tr[1]/td[6]/div/button[3]/div/span").click
    find(:xpath, "/html/body/modal-container/div/div/app-parametros-cobranca-modal-atencao/div[3]/button[2]").click
  end
end

E("clico no botão cancelar da exclusão") do
  sleep(3)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "/html/body/app-root/main/app-parametros-cobranca-table/div/div[2]/div[2]/table/tbody/tr[1]/td[6]/div/button[3]/div/span").click
    find(:xpath, "/html/body/modal-container/div/div/app-parametros-cobranca-modal-atencao/div[3]/button[1]").click
  end
end

Entao("o parametro não deve ser excluido") do
  within_frame(find(:xpath, "//iframe")[:id]) do
    expect(assert_text('Parâmetros de Cobrança - Canais Digitais')).to be(true)
  end  
end