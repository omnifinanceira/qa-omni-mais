Quando('eu clico em simulador') do 
  @home = Home.new
  @home.acessar_simulador
end

Quando('seleciono a loja {string}') do |loja|
  @novaSimulacao.selecionar_loja(loja)
  @novaSimulacao.clicar_em_continuar
end

Quando('seleciono o {string} modelo') do |ano|
  @novaSimulacao.selecionar_ano_modelo(ano)
end

Quando('insiro o valor do veículo {string}') do |valor|
  @novaSimulacao.preencher_valor_veiculo(valor)
end

Quando('insiro o valor do financiado {string}') do |valor|
  @novaSimulacao.preencher_valor_financiado(valor)
end

Então('carrego a simulação') do 
  @novaSimulacao.clicar_em_continuar
  @novaSimulacao.verificar_img_load
end
