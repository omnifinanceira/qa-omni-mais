Quando('eu clico em meu desempenho') do
   @home =  Home.new
   @home.acessar_meu_desempenho
  end

  Quando('valido as opcoes de filtros de desempenho') do
   listamenuprodutos = all(:xpath, "//*[@id='performance']/app-select-produto/app-panel/div/div/app-btn-group/div/div/div/label/span")
      expect((listamenuprodutos[0].text).match('CDC LOJA')).to be_truthy
      expect((listamenuprodutos[1].text).match('CRÉDITO PESSOAL')).to be_truthy
      expect((listamenuprodutos[2].text).match('MICROCREDITO')).to be_truthy
      expect((listamenuprodutos[3].text).match('VEÍCULOS')).to be_truthy

   listamenuperiodo = all(:xpath, "//*[@id='performance']/app-select-periodo/app-panel/div/div/app-btn-group/div/div/div/button")
      expect((listamenuperiodo[0].text).match('Mês')).to be_truthy
      expect((listamenuperiodo[1].text).match('Semana')).to be_truthy
      expect((listamenuperiodo[2].text).match('3 dias')).to be_truthy
         
   expect(page.has_xpath?( "//*[@id='performance']/app-painel-meu-desempenho/app-panel/div")).to eq true
   expect(page.has_xpath?("//*[@id='performance']/app-painel-relacao-fichas/app-panel/div")).to eq true
 end
 
 Quando('clico em Ranking') do
    @home.acessar_ranking
 end
 
 Entao('valido as opcoes de filtro de Ranking') do
   listarankingprodutos = all(:xpath, "//*[@id='ranking']/app-select-produto/app-panel/div/div/app-btn-group/div/div/div/label/span")
     expect((listarankingprodutos[0].text).match('CDC LOJA')).to be_truthy
     expect((listarankingprodutos[1].text).match('CRÉDITO PESSOAL')).to be_truthy
     expect((listarankingprodutos[2].text).match('MICROCREDITO')).to be_truthy
     expect((listarankingprodutos[3].text).match('VEÍCULOS')).to be_truthy

   listarankingperiodo = all(:xpath, "//*[@id='ranking']/app-select-periodo/app-panel/div/div/app-btn-group/div/div/div/button")
      expect((listarankingperiodo[0].text).match('Mês')).to be_truthy
      expect((listarankingperiodo[1].text).match('Ano')).to be_truthy
     
   expect(page.has_xpath?( "//*[@id='ranking']/app-panel/div/div[1]")).to eq true   
 end
  
 