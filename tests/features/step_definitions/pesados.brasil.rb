Dado('que eu esteja logado no omni mais') do
    @Pesados = PesadosBrasil.new
    @novaficha = NovaFicha.new
    @Pesados.load
    @Pesados.logar_usuario
end
 #
Quando('clico na opcao carteira') do
  @Pesados.carteira
end

Quando('pesquiso um cep invalido') do
  @Pesados.campoCepInvalido
 end
 
Quando('eu valido se o texto esta conforme o esperado') do
  @Pesados.validarTexto
end

Quando('pesquiso um cep valido') do
  @Pesados.campoCepValido
 end

 Quando('pesquiso um cep que nao tem agente perto') do
  @Pesados.cepSemAgente
end

Quando('eu preencho a proposta valido o cep') do
  @Pesados.cadastro
end
##############################################################################################
Quando('insiro dado pessoais {string} e {string} do cliente pesado financiamento') do |cpf, nascimento|
  @novaficha.preencher_dados_cliente2(cpf,nascimento, true)
end

 Quando('insiro {int} ou {string} pesados financiamento') do |avalista, conjuge|
  conjuge == "false" ? conjuge  = false : conjuge  = true
  @novaficha.clicar_adicionar_conjuge(conjuge)
  @avalista_preenchido = @novaficha.clicar_adicionar_avalista(avalista)
  @novaficha.clicar_em_continuar
end

# Quando('preencho informacoes do {int} pesados financiamento') do |quantidade_caminhao_proprio|
#   @novaficha.clicar_em_continuar
# end

Quando('insiro dados do veiculos {string} e quantidade de {int} pesado financiamento') do |placa, garantia|
  @novaficha.preencher_dados_veiculo(placa)
  @novaficha.adicionar_garantia(garantia)
  @novaficha.clicar_em_continuar
end

Quando('carrego o resultado parcial pesados financiamento') do
  @novaficha.verificar_img_load
  @novaficha.verificar_pop_up_espera
  @novaficha.clicar_em_continuar
end

Quando('insiro os detalhes das informacoes do cliente pesados financiamento') do
  @novaficha.preencher_detalhes_cliente(true)
  @novaficha.clicar_em_continuar
end

Quando('insiro endereco e observacoes do cliente pesados financiamento') do
  @Pesados.endereco
  @novaficha.clicar_em_continuar
end

Dado('que eu insira um cep valido para consulta') do
  @novaficha.preenche_cep
end

Dado('clico em consultar cep') do
  @novaficha.pesquisarCep
end

Entao('devo ver o botao para cadastrar ficha pesados brasil') do
  expect(@novaficha.encontrarBotaoCadastro).to be_truthy
end

Quando('clico no botao para cadastrar a ficha pesados brasil') do
  @novaficha.cadastrar_ficha_pesados
end

Entao('devo ver o mesmo cep que foi digitado anteriormente') do
  expect(@novaficha.comparaCep).to be_truthy
end

Quando('preencho observacoes Pesados Brasil') do
  @novaficha.observacoes
  @novaficha.clicar_em_continuar
end

Quando('seleciono primeiro caminhao') do
  @novaficha.clicar_em_continuar
end

Quando('insiro mais informacoes do cliente de pesados') do
  @novaficha.preencher_detalhes_cliente("pesados")
  @novaficha.clicar_em_continuar
end

Quando('insiro endereco do cliente Pesados Brasil') do
  @Pesados.endereco_cliente
  @Pesados.clicar_em_continuar
end

Entao('devo ver o popup para confirmacao de envio para o agente de pesados')do
  expect(@Pesados.verifica_pop_up_envio_fila_agente).to be_truthy
end

E('clico para cancelar o envio da proposta para o agente de pesados')do
  @Pesados.cancela_envio_proposta_pesados
end