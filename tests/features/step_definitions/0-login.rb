Quando('eu preencher o campo {string}') do |usuario|
  @login = Login.new
  @login.load
  @login.preencher_usuario(usuario)
end

Quando('eu digitar a {string}') do |senha|
  @login.preencher_senha(senha)
end

Quando('eu clicar no botao Entrar') do
  @login.acessar_conta
end

Quando('eu selecionar o agente') do
  @login.selecionar_agente
end

Quando('eu clicar no botão Confirmar') do
  @login.confirmar_acesso
end

Entao('eu espero visualizar a home do omni mais') do
  textohome = find(:xpath, "//div/h2[@class='record-title']")
  # log(textohome.text)
    expect('Minhas Fichas'.match(textohome.text)).to be_truthy
end

Entao('verifico a {string} de retorno') do |mensagem|
  textofalha = find(:xpath, "//div[@class='toast-message']")
end