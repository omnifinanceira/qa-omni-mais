  Quando('eu clico em Adicionar ficha automoveis financiamento old') do
    #     omnifacil = OmniFacil.new
    # omnifacil.load
    # omnifacil.logar_usuario($usuarioloja,$senhalojista)
    # omnifacil.novo_menu
    # omnifacil.selecionar_menu('OPERACIONAL')
    # omnifacil.selecionar_submenu('CRÉDITO')
    # omnifacil.selecionar_submenu('Fila')
    # #TESTE - AINDA NÃO FUNCIONA.
    # proposta = '31657892'
    # binding.pry
    # omnifacil.consultar_ficha(proposta)
    @home = Home.new
    @home.acessar_ficha
  end
  
  Quando('seleciono o tipo de operacao financiamento para a ficha automoveis old') do
    @novaficha = NovaFicha.new
    @novaficha.pegar_horario
    @novaficha.selecionar_opcao('Financiamento')
  end
  
  Quando('seleciono o solicitante para a ficha automoveis old') do
    @novaficha.selecionar_opcao('VENDEDOR')
    @novaficha.clicar_em_continuar
  end
  
  Quando('seleciono o vendedor automoveis old') do
    @novaficha.pegar_horario
    @novaficha.selecionar_loja('ADRIANO AUTOMOVEIS')
    @novaficha.selecionar_vendedor
    @novaficha.clicar_em_continuar
  end
  
  Quando('insiro dados pessoais {string}, {string} e {string} do cliente automoveis old') do |cpf,nascimento, telefone|
    @novaficha.preencher_dados_cliente(cpf,nascimento,false,telefone)
    @novaficha.clicar_em_continuar
  end
  
  Quando('insiro dados do veiculo {string} automoveis old') do |placa|
    if placa == ""
      sql = Queries.new
      placa =  sql.pegar_placa('leve')
    end
  
    @novaficha.preencher_dados_veiculo_old(placa)
    @novaficha.fechar_alerta_cpf_ficha_existente
    @novaficha.clicar_em_continuar
  end
  
  Quando('carrego o resultado parcial automoveis old') do
    @novaficha.verificar_img_load
    @novaficha.fechar_alerta_parcelamento
  end

  Quando('valido as informacoes inseridas ate o resultado parcial') do
    dados_banco_proposta = @sql.pegar_dados_clientes_proposta($numero_proposta)
  
    dados_banco_proposta.each { |row| expect(!row.nil?).to be_truthy }
  end
  
  Quando('ajusto o valor financiado e quantidade de parcelas automoveis old') do
    @novaficha.selecionar_valor_financiamento_old
    @novaficha.selecionar_parcelas
    @novaficha.pegar_dados_financiamento
    @novaficha.clicar_em_continuar
    @novaficha.fechar_pop_up_alerta_estrelas
  end
  
  Quando('insiro mais informacoes e {string} do cliente automoveis old') do |email|
    @novaficha.preencher_detalhes_cliente(false,true,email)
    @novaficha.clicar_em_continuar
  end
  
  Quando('insiro endereco do cliente automoveis old') do
    @novaficha.endereco_cliente(true)
    @novaficha.clicar_em_continuar
  end
  
  Quando('preencho observacoes automoveis old') do
    @novaficha.observacoes
    @novaficha.clicar_em_continuar
    @novaficha.verificar_img_load_analise
  end
  
  Entao('valido se a ficha foi criada e enviada para analise automoveis old') do
    teste = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text

    texto_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text.split("\n")
      expect((texto_sucesso[0]).match('A SOLICITAÇÃO FOI FINALIZADA')).to be_truthy
      expect((texto_sucesso[1]).match('E ENVIADA PARA ANÁLISE')).to be_truthy
  end
  
  Entao('validar os dados da proposta automoveis old') do
    dados_proposta_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[4]/div/p").text.split(" ")
      expect((dados_proposta_sucesso[2].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
      expect((dados_proposta_sucesso[4]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy
  end

  # Entao('avalio proposta no omnifacil') do
  #   omnifacil = OmniFacil.new
  #   omnifacil.load
  #   omnifacil.logar_usuario($usuarioloja,$senhalojista)
  #   # omnifacil.novo_menu
  #   omnifacil.selecionar_menu('OPERACIONAL')
  #   omnifacil.selecionar_submenu('CRÉDITO')
  #   omnifacil.selecionar_submenu('Fila')
  #   #TESTE - AINDA NÃO FUNCIONA.
  #   proposta = '26363657'
  #   binding.pry
  #   omnifacil.consultar_ficha(proposta)
  # end