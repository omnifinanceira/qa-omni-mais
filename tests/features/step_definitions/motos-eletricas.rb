require 'pry'


Quando('seleciono a {string} do veículo') do |categoria|
    @motoeletrica.selecionar_categoria(categoria)
end

Quando('seleciono o {int} do veículo') do |ano|
    @motoeletrica.selecionar_ano_modelo(ano)
end

Quando('seleciono o {string} do veículo') do |versao|
    @motoeletrica.selecionar_versao(versao)
end

Quando('seleciono a {string} do veiculo') do |cor|
    @motoeletrica.selecionar_cor(cor)
end


Então('seleciono o {string} do veiculo') do |combustivel|
    @motoeletrica.selecionar_tipo_combustivel(combustivel)
    #expect(combustivel).to eq "ELÉTRICO"
    sleep(5)
end

Quando('seleciono o {string} de Licenciamento do veículo') do |estado|
    @motoeletrica.selecionar_uf(estado)
    @motoeletrica.clicar_em_continuar
end



Então('valido se a ficha foi criada e enviada para analise') do
    @novaficha = NovaFicha.new
    numero_proposta = @novaficha.pegar_numero_ficha
    puts @novaficha
    sleep(10)
    teste = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text

    texto_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text.split("\n")
    expect((texto_sucesso[0]).match("A FICHA #{$numero_proposta} FOI FINALIZADA")).to be_truthy
    expect((texto_sucesso[1]).match('E ENVIADA PARA ANÁLISE')).to be_truthy
end

Então('deve listar a opção ‘Elétrico’ em Tipo de Combustível.') do
    @motoeletrica.verifica_tipo_combustivel
end

Quando('informo uma placa invalida') do
    @motoeletrica.inserir_placa_invalida
end

Então ('devo ver uma mensagem informando codigo FIPE nao encontrado')do 
    @motoeletrica.verifica_fipe_nao_encontrado
end

Quando('clico para alterar a comissao da proposta')do
    @motoeletrica.clica_comissao
end

Quando('altero a comissao da proposta')do
    @motoeletrica.altera_comissao
end

Quando('verifico se o valor mudou apos alterar a comissao')do
    expect($valorParcelaInicio.eql?($valorParcelaFim)).to be_falsey
end

Quando('verifico que a comissao se manteve')do
    expect($valorParcelaInicio.eql?($valorParcelaFim)).to be_truthy
end

Quando('verifico se o seguro esta disponivel')do
    expect(page.has_css?('h3', text: 'OMNI PROTEÇÃO FINANCEIRA - GERAL')).to be_falsey
end

Entao('acesso o omnifacil e a fila do agente para verificar o seguro') do
    @omnifacil.load
    @omnifacil.logar_usuario($usuarioagente1,$senhaagente1)
    @omnifacil.selecionar_menu_inicial(" NOVO MENU")
    @omnifacil.selecionar_agente
    @omnifacil.selecionar_menu('OPERACIONAL')
    @omnifacil.selecionar_submenu('CRÉDITO')
    @omnifacil.selecionar_submenu('Fila')
  end

Quando('acesso a proposta para aprovacao')do
    @erro = @omnifacil.consultar_ficha($numero_proposta)

        while @erro.include?("\"//input[contains(@onclick,'#{$numero_proposta}')]\"")
            @omnifacil.selecionar_menu('OPERACIONAL')
            @omnifacil.selecionar_submenu('CRÉDITO')
            @omnifacil.selecionar_submenu('Fila')

            @omnifacil.marcar_proposta_omni_mais

            @erro = @omnifacil.consultar_ficha($numero_proposta)
        end
end

Quando('seleciono o menu Ficha\/Verificações')do
    @omnifacil.seleciona_ficha_verificacoes
end

Quando('clico no botao de seguros')do
    @omnifacil.abrir_seguros
end

Quando('valido se tem seguros disponiveis')do
    @omnifacil.verificar_seguros
    expect(($msgVerificaSeguro).match('Não existem Seguros parametrizados para oferta !')).to be_truthy
end