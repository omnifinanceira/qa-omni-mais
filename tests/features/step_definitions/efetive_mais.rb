require 'pry'

Dado('que eu esteja logado no omni facil') do
 @efetive = EfetiveMais.new
 @omnifacil = LoginOmniFacil.new
 @omnifacil.load
 @omnifacil.logar_usuario
end

Quando('clico no novo menu') do
  @efetive.novo_menu
end

Quando('seleciono a aba operacional') do
  @efetive.selecionar_menu
end

 Quando('carrego a pagina') do
  @efetive.tela_efetive
 end

 Quando('seleciono produto moto') do
  @efetive.preencher_filtro_efetive_mais
 end
 
 Quando('seleciono produto moto com filtros') do
  @efetive.produto_moto1
 end

 Quando('seleciono produto veiculo leve') do
  @efetive.produto_veiculo_leve
end

Quando('seleciono produto veiculo leve com filtros') do
  @efetive.produto_veiculo_leve1
end

Quando('seleciono produto veiculo pesado') do
  @efetive.produto_veiculo_pesado
end

Quando('seleciono produto todos') do
  @efetive.produto_todos
end

Quando('clico no botao limpar') do
  @efetive.btn_limpar
end

Quando('clico no botao pesquisar letra') do
  @efetive.btn_pesquisar_letra
end

Quando('clico no botao pesquisar numero') do
  @efetive.btn_pesquisar_numero
end

Quando('coloco o valor a ser financiado') do
  @efetive.valor_financiar
end

Quando('verifico o score') do
  @efetive.score
end

Quando('clico no mesmo agente') do
  @efetive.agente
end
 
Quando('capturo uma proposta de agente diferente') do
  @efetive.capturaProposta
end

Quando('preencho o filtro de pesquisa com as informacoes {string}, {string}, {string}, {string}, {string}, {string}') do |produto, ano, estado, cidade, entrada, compRenda|
  @efetive.preencher_filtro_efetive_mais(produto, ano, estado, cidade, entrada, compRenda)
end

Dado('que eu esteja logado no OmniFacil para acessar a mesa de credito')do
  @efetive = EfetiveMais.new
  @loginOmnifacil = LoginOmniFacil.new
  @loginOmnifacil.load
  @loginOmnifacil.logar_usuario($usuarioAprovaMesa, $senhaAprovaMesa)
end

E('seleciono a mesa de credito')do
  @omnifacil = OmniFacil.new
  @omnifacil.selecionar_menu_inicial("MESA DE CRÉDITO ")
end

E('seleciono a mesa de veiculos leves')do
  @sql = Queries.new
  cod_mesa_proposta  = @sql.localizar_codigo_mesa($numero_proposta.to_i)
  @omnifacil.acessar_grupo_proposta_mesa(cod_mesa_proposta)
end

E('aprovo a proposta enviada para a mesa')do
  @omnifacil.aprovar_proposta_com_pendencia($numero_proposta)
end