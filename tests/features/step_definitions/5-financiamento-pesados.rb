Quando('eu clico em Adicionar ficha pesado financiamento') do
    @sql = Queries.new
    @novaficha = NovaFicha.new
    @home = Home.new
    @home.acessar_ficha
  end

  Quando('seleciono o {string}, {string} e {string} pesado financiamento') do |operacao, produto, solicitante|
    @novaficha.pegar_horario
    @novaficha.selecionar_opcao(operacao)
    @novaficha.selecionar_opcao(produto)
    @novaficha.selecionar_opcao(solicitante)
    @novaficha.clicar_em_continuar
  end
  
  Quando('seleciono a {string} e o vendedor pesado financiamento') do |loja|
    @novaficha.selecionar_loja(loja)
    @novaficha.selecionar_vendedor
    @novaficha.clicar_em_continuar
  end

  Quando('insiro dados pessoais {string} e {string} do cliente pesado financiamento') do |cpf, nascimento|
    @novaficha.preencher_dados_cliente(cpf,nascimento, true)
  end

  Quando('insiro {int} ou {string} pesado financiamento') do |avalista, conjuge|
    conjuge == "false" ? conjuge  = false : conjuge  = true
    @novaficha.clicar_adicionar_conjuge(conjuge)
    @avalista_preenchido = @novaficha.clicar_adicionar_avalista(avalista)
    @novaficha.clicar_em_continuar
  end

  Quando('preencho informacoes do {int} pesado financiamento') do |quantidade_caminhao_proprio|
    # quantidade_caminhao_proprio == 0 ? caminhao_proprio  = false : caminhao_proprio  = true
    # @caminhao_proprio_preenchido = @novaficha.marcar_caminhao_proprio(caminhao_proprio,quantidade_caminhao_proprio)
    @novaficha.clicar_em_continuar
  end

  Quando('insiro dados do veiculo {string} e quantidade de {int} pesado financiamento') do |placa, garantia|
    @novaficha.preencher_dados_veiculo(placa)
    @novaficha.adicionar_garantia(garantia)
    @novaficha.clicar_em_continuar
  end

  Quando('carrego o resultado parcial pesado financiamento') do
   # binding.pry
    @novaficha.verificar_img_load
    @novaficha.verificar_pop_up_espera
  end

  Quando('valido as informacoes inseridas ate o resultado parcial pesado financiamento') do
    $numero_proposta = current_url.split('/').last
    dados_clientes_proposta = @sql.pegar_dados_clientes_proposta($numero_proposta)
    dados_clientes_proposta.each { |row| expect(!row.nil?).to be_truthy }
  
    dados_proposta_banco = @sql.pegar_dados_proposta_banco($numero_proposta)
    dados_proposta_banco.each { |row| expect(!row.nil?).to be_truthy }
  
    dados_clientes_telefones_proposta = @sql.pegar_dados_clientes_telefones_proposta($numero_proposta)
    dados_clientes_telefones_proposta.each { |row| expect(!row.nil?).to be_truthy }
  end
  
  Quando('ajusto o seguro, valor financiado e quantidade de parcelas pesado financiamento') do
    @novaficha.fechar_alerta_parcelamento
    # @novaficha.alterar_retorno_seguro_assistencia
    @novaficha.selecionar_valor_financiamento
    @novaficha.selecionar_parcelas
    @novaficha.pegar_dados_financiamento_simulador
    @novaficha.clicar_em_continuar
    @novaficha.fechar_pop_up_alerta_estrelas
  end
  
  Quando('insiro os detalhes das informacoes do cliente pesado financiamento') do
    @novaficha.preencher_detalhes_cliente(true)
    @novaficha.clicar_em_continuar
  end
  
   Quando('insiro endereco e observacoes do cliente pesado financiamento') do
    @novaficha.endereco_cliente
    @novaficha.clicar_em_continuar
    @novaficha.observacoes
  end
  
  Quando('finalizo a proposta e valido as informacoes financiamento pesado financiamento') do
    @novaficha.clicar_em_continuar
    @novaficha.verificar_img_load_analise

    @novaficha.pegar_dados_financiamento_finalizar

    texto_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text.split("\n")
      expect((texto_sucesso[0]).match("A FICHA #{$numero_proposta} FOI FINALIZADA")).to be_truthy
      expect((texto_sucesso[1]).match('E ENVIADA PARA ANÁLISE')).to be_truthy

    dados_proposta_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[4]/div/p").text.split(" ")
      expect((dados_proposta_sucesso[2].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
      expect((dados_proposta_sucesso[4]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy
    
    @novaficha.captura_numero_proposta
    numero_proposta = find(:xpath, "//span[@class='id-proposta-info']").text
      expect((current_url.split('/').last).match(numero_proposta)).to be_truthy
  
    expect(find(:xpath, "//a[@href='/ficha'][text()='OK, ir para home ']").visible?).to be_truthy

    dados_clientes_fisica_proposta = @sql.pegar_dados_clientes_fisica_proposta($numero_proposta)
    dados_clientes_fisica_proposta.each { |row| expect(!row.nil?).to be_truthy }
  
    dados_clientes_enderecos_proposta = @sql.pegar_dados_clientes_enderecos_proposta($numero_proposta)
    dados_clientes_enderecos_proposta.each { |row| expect(!row.nil?).to be_truthy }
  end

  Quando('informacoes dos caminhoes proprios do cliente financiamento pesado financiamento') do
    @novaficha.preencher_informacoes_caminhoes(@caminhao_proprio_preenchido,false,true)
    @novaficha.clicar_em_continuar
  end

  Quando('informo os detalhes e endereco do avalista financiamento pesado financiamento') do
    @novaficha.preencher_detalhes_avalista(@avalista_preenchido)
  end

  Quando('preencho novo avalista financiamento pesado financiamento') do
    @novaficha.clicar_adicionar_avalista_observacoes
    @novaficha.preencher_avalista_observacoes
    @novaficha.clicar_salvar
  end

  Entao('acesso o omnifacil e a fila do agente') do
    @omnifacil = OmniFacil.new
    @omnifacil.load
    @omnifacil.logar_usuario($usuarioagente1,$senhaagente1)
    @omnifacil.selecionar_menu_inicial(" NOVO MENU")
    @omnifacil.selecionar_agente
    @omnifacil.selecionar_menu('OPERACIONAL')
    @omnifacil.selecionar_submenu('CRÉDITO')
    @omnifacil.selecionar_submenu('Fila')
  end

  Entao('acesso o omnifacil e a fila do agente para alterar o valor liberado') do
    @omnifacil.load
    @omnifacil.logar_usuario($usuarioagente1,$senhaagente1)
    @omnifacil.selecionar_menu_inicial(" NOVO MENU")
    @omnifacil.selecionar_agente
    @omnifacil.selecionar_menu('OPERACIONAL')
    @omnifacil.selecionar_submenu('CRÉDITO')
    @omnifacil.selecionar_submenu('Fila')
  end
  
  Entao('marco o filtro de proposta criada no omni mais') do
    @omnifacil.marcar_proposta_omni_mais
  end

  Entao('acesso a proposta, aprovo etapa historica e ficha verificacoes') do
    @omnifacil = OmniFacil.new
    @erro = @omnifacil.consultar_ficha($numero_proposta)
    @var_proposta = $numero_proposta
    while @erro.include?("\"//input[contains(@onclick,'#{$numero_proposta}')]\"")
        @omnifacil.selecionar_menu('OPERACIONAL')
        @omnifacil.selecionar_submenu('CRÉDITO')
        @omnifacil.selecionar_submenu('Fila')

        @omnifacil.marcar_proposta_omni_mais

        @erro = @omnifacil.consultar_ficha($numero_proposta)
    end

    #aprovar Histórica
    @omnifacil.acessar_etapa("Histórica")
    @omnifacil.aceitar_etapa

    #aprovar Ficha/Verificações
    @omnifacil.acessar_etapa("Ficha/Verificações")

    dados_omnifacil = @omnifacil.pegar_valor_prazo_omnifacil
      expect((dados_omnifacil[0]).match($valor_parcela_omni_mais)).to be_truthy
      expect((dados_omnifacil[1]).match($quantidade_parcela_omni_mais)).to be_truthy
      
    @omnifacil.clicar_gravar
    @omnifacil.aceitar_etapa
  end

  Entao('acesso a proposta, aprovo etapa historica e ficha verificacoes e altero o valor liberado') do
    @erro = @omnifacil.consultar_ficha($numero_proposta)

    while @erro.include?("\"//input[contains(@onclick,'#{$numero_proposta}')]\"")
        @omnifacil.selecionar_menu('OPERACIONAL')
        @omnifacil.selecionar_submenu('CRÉDITO')
        @omnifacil.selecionar_submenu('Fila')

        @omnifacil.marcar_proposta_omni_mais

        @erro = @omnifacil.consultar_ficha($numero_proposta)
    end

    #aprovar Histórica
    @omnifacil.acessar_etapa("Histórica")
    @omnifacil.aceitar_etapa

    #aprovar Ficha/Verificações
    @omnifacil.acessar_etapa("Ficha/Verificações")

    dados_omnifacil = @omnifacil.pegar_valor_prazo_omnifacil_alterado
      #expect((dados_omnifacil[0]).match($valor_parcela_omni_mais)).to be_truthy
      #expect((dados_omnifacil[1]).match($quantidade_parcela_omni_mais)).to be_truthy
      
    @omnifacil.clicar_gravar
    @omnifacil.aceitar_etapa
  end

  Entao('acesso a etapa de decisao, envio a proposta para a mesa e preencho o checklist') do
    @omnifacil.acessar_etapa("Decisão")
    @omnifacil.enviar_mesa_omni

    @omnifacil.preencher_checklist
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)

    texto_confirma_envio_mesa = page.find(:xpath, "//form/center/font[@class='txtAzulBold12']").text
      expect(("Dados enviados com sucesso!\nProposta:#{$numero_proposta} - DEVOLVIDA").match(texto_confirma_envio_mesa)).to be_truthy
  end

  Entao('acesso a mesa, proposta, aprovo e verifico') do
    @omnifacil.load
    @omnifacil.logar_usuario("SILVANA_MONTEIRO","PTER0IKDCB")
    @omnifacil.selecionar_menu_inicial("MESA DE CRÉDITO - (Nova Fila) ")

    cod_mesa_proposta  = @sql.localizar_codigo_mesa($numero_proposta.to_i)
    @omnifacil.acessar_grupo_proposta_mesa(cod_mesa_proposta)
    @texto_confirma_proposta_aprovada = @omnifacil.aprovar_proposta_mesa($numero_proposta.to_i)

    # expect(("Dados enviados com sucesso!\nProposta:#{$numero_proposta} - Enviada para fila de Aprovação do Supervisor.").match(@texto_confirma_proposta_aprovada)).to be_truthy
  end

  Dado('volto a logar no omnimais para emitir o pre contrato') do
    @login = Login.new
    @login.load
    @login.acessar_conta_omnimais_loja
  end

  Dado('acesso as fichas aprovadas') do
    @novaficha = NovaFicha.new
    @novaficha.acessar_proposta_aprovada_omnimais($numero_proposta)
  end

  Dado('emito o pre contrato') do
    @novaficha.emissao_pre_contrato
  end

  Dado('anexo os documentos e envio') do
    @novaficha.anexo_documentos
    
  end

  Dado('acesso a proposta') do
      
    @erro = @omnifacil.consultar_ficha(@var_proposta)
   
    while @erro.include?("\"//input[contains(@onclick,'#{@var_proposta}')]\"")
        @omnifacil.selecionar_menu('OPERACIONAL')
        @omnifacil.selecionar_submenu('CRÉDITO')
        @omnifacil.selecionar_submenu('Fila')

        @omnifacil.marcar_proposta_omni_mais

        @erro = @omnifacil.consultar_ficha(@var_proposta)
    end


  end

  Entao('preencho os dados para liberacao do pagamento') do
    @omnifacil.preencher_dados_pagamento
  end
   

  