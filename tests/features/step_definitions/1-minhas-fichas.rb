Dado('que eu esteja logado como agente') do
    @login = Login.new
    @login.load
    @login.acessar_conta_omnimais_agente
    @login.selecionar_batistella
end

Quando('eu espero visualizar minhas fichas') do
    page.assert_selector(:xpath, "//app-ficha-em-analise/ol/li/app-ficha-card", wait:15)
    @minhasfichas = all(:xpath, "//li/app-ficha-card/div/div[@class='body']/div[contains(@class, 'infos')]//div[text()]")
    @clientes = all(:xpath, "//li/app-ficha-card/div/div[@class='body']/div[contains(@class, 'infos')]/div[@class='row client']")
    @operators = all(:xpath, "//li/app-ficha-card/div/div[@class='body']/div[contains(@class, 'infos')]/div[@class='row operator']")
end

Entao('valido as informacoes em cada ficha') do
    expect(@minhasfichas[0].text).to include('Ficha')
    expect(@minhasfichas[1].text).to include('Iniciada')
    expect(@clientes[0].text).to include('Cliente')
    expect(@clientes[0].text).to include('CPF:')
    expect(@operators[0].text).not_to be_empty
end