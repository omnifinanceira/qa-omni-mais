Quando('eu clico em Adicionar ficha pesado refinanciamento') do
  @sql = Queries.new
  @novaficha = NovaFicha.new
  @home = Home.new
  @home.acessar_ficha
end

Quando('seleciono o {string}, {string} e {string} pesado refinanciamento') do |operacao, produto, solicitante|
  @novaficha.pegar_horario
  @novaficha.selecionar_opcao(operacao)
  @novaficha.selecionar_opcao(produto)
  @novaficha.selecionar_opcao(solicitante)
  @novaficha.clicar_em_continuar
end

Quando('seleciono a {string} e o vendedor pesado refinanciamento') do |loja|
  @novaficha.selecionar_loja(loja)
  @novaficha.selecionar_vendedor
  @novaficha.clicar_em_continuar
end

Quando('insiro dados pessoais {string} e {string} do cliente pesado refinanciamento') do |cpf, nascimento|
  @novaficha.preencher_dados_cliente(cpf,nascimento, true)
end

Quando('insiro {float} ou {string} pesado refinanciamento') do |avalista, conjuge|
  conjuge == "false" ? conjuge  = false : conjuge  = true
  @novaficha.clicar_adicionar_conjuge(conjuge)
  @avalista_preenchido = @novaficha.clicar_adicionar_avalista(avalista)
  @novaficha.clicar_em_continuar
end

Quando('preencho informacoes do {int} pesado refinanciamento') do |quantidade_caminhao_proprio|
  @novaficha.clicar_em_continuar
end

Quando('insiro dados do veiculo {string} e quantidade de {int} pesado refinanciamento') do |placa, garantia|
  @novaficha.preencher_dados_veiculo(placa)
  @novaficha.adicionar_garantia(garantia)
  @novaficha.clicar_em_continuar
end

Quando('carrego o resultado parcial pesado refinanciamento') do
   @novaficha.verificar_img_load
   @novaficha.verificar_pop_up_espera
 end

 Quando('valido as informacoes inseridas ate o resultado parcial pesado refinanciamento') do
  $numero_proposta = current_url.split('/').last
  dados_clientes_proposta = @sql.pegar_dados_clientes_proposta($numero_proposta)
  dados_clientes_proposta.each { |row| expect(!row.nil?).to be_truthy }

  dados_proposta_banco = @sql.pegar_dados_proposta_banco($numero_proposta)
  dados_proposta_banco.each { |row| expect(!row.nil?).to be_truthy }

  dados_clientes_telefones_proposta = @sql.pegar_dados_clientes_telefones_proposta($numero_proposta)
  dados_clientes_telefones_proposta.each { |row| expect(!row.nil?).to be_truthy }
end

Quando('ajusto o seguro, valor financiado e quantidade de parcelas pesado refinanciamento') do
  @novaficha.fechar_alerta_parcelamento
  # @novaficha.alterar_retorno_seguro_assistencia
  @novaficha.selecionar_valor_financiamento
  @novaficha.selecionar_parcelas
  @novaficha.pegar_dados_financiamento_simulador
  @novaficha.clicar_em_continuar
  @novaficha.fechar_pop_up_alerta_estrelas
end

Quando('insiro os detalhes das informacoes do cliente pesado refinanciamento') do
  @novaficha.preencher_detalhes_cliente(true)
  @novaficha.clicar_em_continuar
end

 Quando('insiro endereco e observacoes do cliente pesado refinanciamento') do
  @novaficha.endereco_cliente
  @novaficha.clicar_em_continuar
  @novaficha.observacoes
end

Quando('finalizo a proposta e valido as informacoes refinanciamento pesado refinanciamento') do
  @novaficha.clicar_em_continuar
  @novaficha.verificar_img_load_analise

  @novaficha.pegar_dados_financiamento_finalizar

  texto_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text.split("\n")
    expect((texto_sucesso[0]).match("A FICHA #{$numero_proposta} FOI FINALIZADA")).to be_truthy
    expect((texto_sucesso[1]).match('E ENVIADA PARA ANÁLISE')).to be_truthy

  dados_proposta_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[4]/div/p").text.split(" ")
    expect((dados_proposta_sucesso[2].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
    expect((dados_proposta_sucesso[4]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy

  numero_proposta = find(:xpath, "//span[@class='id-proposta-info']").text
    expect((current_url.split('/').last).match(numero_proposta)).to be_truthy

  expect(find(:xpath, "//a[@href='/ficha'][text()='OK, ir para home ']").visible?).to be_truthy

  dados_clientes_fisica_proposta = @sql.pegar_dados_clientes_fisica_proposta($numero_proposta)
  dados_clientes_fisica_proposta.each { |row| expect(!row.nil?).to be_truthy }

  dados_clientes_enderecos_proposta = @sql.pegar_dados_clientes_enderecos_proposta($numero_proposta)
  dados_clientes_enderecos_proposta.each { |row| expect(!row.nil?).to be_truthy }
end

Quando('informacoes dos caminhoes proprios do cliente refinanciamento pesado refinanciamento') do
  @novaficha.preencher_informacoes_caminhoes(@caminhao_proprio_preenchido,false,true)
  @novaficha.clicar_em_continuar
end

Quando('informo os detalhes e endereco do avalista refinanciamento pesado refinanciamento') do
  @novaficha.preencher_detalhes_avalista(@avalista_preenchido)
end

Quando('preencho novo avalista refinanciamento pesado refinanciamento') do
  @novaficha.clicar_adicionar_avalista_observacoes
  @novaficha.preencher_avalista_observacoes
  @novaficha.clicar_salvar
end






















# Quando('seleciono a loja pesado refinanciamento') do
#   @novaficha.selecionar_loja
# end

# Quando('seleciono o vendedor pesado refinanciamento') do
#   @novaficha.pegar_horario
#   @novaficha.selecionar_vendedor
#   @novaficha.clicar_em_continuar
# end


# Quando('preencho avalista refinanciamento') do
#   @novaficha.clicar_adicionar_avalista
#   @novaficha.preencher_avalista_cliente
#   @novaficha.preencher_veiculo_avalista
#   @novaficha.clicar_em_continuar
# end

# Quando('preencho informacoes do caminhao proprio refinanciamento') do
#   placa =  @sql.pegar_placa('pesado')
#   @novaficha.marcar_caminhao_proprio_sim(true)
#   @novaficha.quantidade_caminhao_proprio
#   @novaficha.placa_caminhao_proprio(placa)
#   @novaficha.clicar_em_continuar
# end

# Quando('insiro dados do veiculo {string} pesado refinanciamento') do |placa|
#   @novaficha.preencher_dados_veiculo(placa)
#   @novaficha.fechar_alerta_cpf_ficha_existente
#   @novaficha.clicar_em_continuar
# end

# Quando('carrego o resultado parcial pesado refinanciamento') do
#   @novaficha.verificar_img_load
#   @novaficha.fechar_alerta_parcelamento
# end

# Quando('ajusto o valor financiado e quantidade de parcelas pesado refinanciamento') do
#   @novaficha.pegar_numero_ficha

#   # dados_banco_proposta = @sql.pegar_dados_proposta_banco($numero_proposta)

#   #DADOS PROPOSTA
#   # dados_banco_proposta.each { |row| expect(!row.nil?).to be_truthy }

#   @novaficha.selecionar_valor_refinanciamento
#   @novaficha.selecionar_parcelas
#   @novaficha.pegar_dados_refinanciamento
#   @novaficha.clicar_em_continuar
# end

# Quando('insiro as informacoes de detalhes cliente refinanciamento') do
#   @novaficha.preencher_detalhes_cliente(true)
#   @novaficha.clicar_em_continuar
# end

# Quando('informacoes dos caminhoes proprios do cliente refinanciamento') do
#   @novaficha.preencher_informacoes_caminhoes(false,true)
#   @novaficha.clicar_em_continuar
# end

# Quando('informo os detalhes do avalista refinanciamento') do
#   @novaficha.preencher_detalhes_avalista
#   @novaficha.clicar_em_continuar
# end

# Quando('insiro endereco do cliente pesado refinanciamento') do
#   @novaficha.endereco_cliente
#   @novaficha.clicar_em_continuar
# end

# Quando('informo o endereco do avalista refinanciamento') do
#   @novaficha.endereco_avalista
#   @novaficha.clicar_em_continuar
# end

# Quando('preencho observacoes pesado refinanciamento') do
#   @novaficha.observacoes
# end

# Quando('preencho novo avalista refinanciamento') do
#   @novaficha.clicar_adicionar_avalista_observacoes
#   @novaficha.preencher_avalista_observacoes
#   @novaficha.clicar_salvar
# end

# Quando('finalizo a proposta refinanciamento') do
#   @novaficha.clicar_em_continuar
#   @novaficha.verificar_img_load_analise
# end

# Entao('valido se a ficha foi criada e enviada para analise pesado refinanciamento') do
#   teste = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text

#   texto_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text.split("\n")
#     expect((texto_sucesso[0]).match('A SOLICITAÇÃO FOI FINALIZADA')).to be_truthy
#     expect((texto_sucesso[1]).match('E ENVIADA PARA ANÁLISE')).to be_truthy
# end

# Entao('validar os dados da proposta pesado refinanciamento') do
#   dados_proposta_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[4]/div/p").text.split(" ")
#   expect((dados_proposta_sucesso[2].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
#   expect((dados_proposta_sucesso[4]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy

# find(:xpath, "//a[@href='/ficha'][text()='OK, ir para home ']").click
# end