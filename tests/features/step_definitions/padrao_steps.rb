Dado('que eu esteja logado como lojista') do
    @login = Login.new
    @login.load
    @login.acessar_conta_omnimais_loja
    @sql = Queries.new
    @omnifacil = OmniFacil.new
    @novaficha = NovaFicha.new
end

Quando('eu clico em Adicionar ficha') do
    @home = Home.new
    @home.acessar_ficha
end

Quando('insiro dados pessoais {string} e {string} do cliente com extrato') do |cpf, dataNascimento|
    @novaficha.preencher_dados_cliente(cpf, dataNascimento, true)
    @novaficha.clicar_em_continuar
end

Quando('insiro dados pessoais {string} e {string} do cliente') do |cpf, dataNascimento|
    @novaficha.preencher_dados_cliente(cpf, dataNascimento)
    @novaficha.clicar_em_continuar
end

Quando('seleciono o nome do vendedor') do
    @novaficha.selecionar_vendedor
    @novaficha.clicar_em_continuar
end

Quando('insiro endereco do cliente old') do
    @novaficha.endereco_cliente(true)
    @novaficha.clicar_em_continuar
end

Quando('seleciono a loja {string} e o vendedor') do |loja|
    @novaficha.selecionar_loja(loja)
    @novaficha.selecionar_vendedor
    @novaficha.clicar_em_continuar
end

Quando('seleciono veiculo 0km') do
    @novaficha.veiculo_zero
end

Quando('seleciono as informacoes do veiculo {string},{string},{string},{string},{string},{string},{string},{string}') do |categoria,marca,ano,modelo,versao,cor,combustivel,estado|
    @novaficha.preenche_info_veiculo(categoria,marca,ano,modelo,versao,cor,combustivel,estado)
end

Quando('seleciono o {string}') do |operacao|
    @novaficha.selecionar_opcao(operacao)
    @novaficha.clicar_em_continuar
end

Quando('seleciono o {string}, {string} e {string}') do |operacao, produto, solicitante|
    @novaficha.selecionar_opcao(operacao)
    @novaficha.selecionar_opcao(produto)
    @novaficha.selecionar_opcao(solicitante)
    @novaficha.clicar_em_continuar
end

Quando('carrego o resultado parcial') do
    @novaficha.verificar_img_load
    binding.pry
    @novaficha.fechar_pop_up_parcelas
    @novaficha.selecionar_valor_financiamento
    @novaficha.clicar_em_continuar
    @novaficha.fechar_pop_up_alerta_estrelas
end


Quando('insiro mais informacoes do cliente') do
    @novaficha.preencher_detalhes_cliente
    @novaficha.clicar_em_continuar
end

Quando('insiro endereco do cliente') do
    @novaficha.endereco_cliente
    @novaficha.clicar_em_continuar
end

Quando('preencho observacoes') do
    @novaficha.observacoes
    @novaficha.clicar_em_continuar
end

Entao('devo ver a tela de observacoes')do
    expect(@novaficha.verifica_observacoes).to be_truthy
end
