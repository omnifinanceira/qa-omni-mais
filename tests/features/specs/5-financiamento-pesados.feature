#language:pt
@5 @financiamento-pesados
Funcionalidade: Criar nova proposta financiamento pesados

Contexto:
    Dado que eu esteja logado como lojista

@pesado-finalizar-proposta
Cenario: Criar nova proposta financiamento pesados - etapa inicial omni+

Quando eu clico em Adicionar ficha pesado financiamento
E seleciono o "Financiamento", "Pesados" e "LOJISTA" pesado financiamento
E seleciono a "2 P VEICULOS" e o vendedor pesado financiamento
E insiro dados pessoais <cpf> e <nascimento> do cliente pesado financiamento
E insiro <avalista> ou <conjuge> pesado financiamento
E preencho informacoes do <caminhaoproprio> pesado financiamento
E insiro dados do veiculo <placa> e quantidade de <garantia> pesado financiamento
E carrego o resultado parcial pesado financiamento
E valido as informacoes inseridas ate o resultado parcial pesado financiamento
E ajusto o seguro, valor financiado e quantidade de parcelas pesado financiamento
E informo os detalhes e endereco do avalista financiamento pesado financiamento
E insiro os detalhes das informacoes do cliente pesado financiamento
E insiro endereco e observacoes do cliente pesado financiamento
E finalizo a proposta e valido as informacoes financiamento pesado financiamento

Exemplos:
    |cpf          |nascimento  |placa    |caminhaoproprio|garantia|avalista|conjuge|
    #|"04333332869"|"06/08/1962"|"DTB9C03"|0              |0       |0       |"false"|
    |"00507942809"|"22/12/1959"|"MBI1154"|0              |0       |0       |"false"|

@pesado-mesa
Cenario: Criar nova proposta financiamento pesados - aprovacao mesa

Quando eu clico em Adicionar ficha pesado financiamento
E seleciono o "Financiamento", "Pesados" e "LOJISTA" pesado financiamento
E seleciono a "2 P VEICULOS" e o vendedor pesado financiamento
E insiro dados pessoais <cpf> e <nascimento> do cliente pesado financiamento
E insiro <avalista> ou <conjuge> pesado financiamento
E preencho informacoes do <caminhaoproprio> pesado financiamento
E insiro dados do veiculo <placa> e quantidade de <garantia> pesado financiamento
E carrego o resultado parcial pesado financiamento
E valido as informacoes inseridas ate o resultado parcial pesado financiamento
E ajusto o seguro, valor financiado e quantidade de parcelas pesado financiamento
E informo os detalhes e endereco do avalista financiamento pesado financiamento
E insiro os detalhes das informacoes do cliente pesado financiamento
E insiro endereco e observacoes do cliente pesado financiamento
E finalizo a proposta e valido as informacoes financiamento pesado financiamento
Entao acesso o omnifacil e a fila do agente 
E marco o filtro de proposta criada no omni mais
E acesso a proposta, aprovo etapa historica e ficha verificacoes
E acesso a etapa de decisao, envio a proposta para a mesa e preencho o checklist
E acesso a mesa, proposta, aprovo e verifico


Exemplos:
    |cpf          |nascimento  |placa    |caminhaoproprio|garantia|avalista|conjuge|
    #|"04333332869"|"06/08/1962"|"DTB9C03"|0              |0       |0       |"false"|
    |"00507942809"|"22/12/1959"|"MBI1154"|0              |0       |0       |"false"|


@cenarioTeste
Cenario: Aprovacao mesa

Entao acesso o omnifacil e a fila do agente 
E marco o filtro de proposta criada no omni mais
E acesso a proposta, aprovo etapa historica e ficha verificacoes
E acesso a etapa de decisao, envio a proposta para a mesa e preencho o checklist
E acesso a mesa, proposta, aprovo e verifico

@pesado-pre-contrato
Cenario: Criar nova proposta financiamento pesados - geracao pre contrato

Quando eu clico em Adicionar ficha pesado financiamento
E seleciono o "Financiamento", "Pesados" e "LOJISTA" pesado financiamento
E seleciono a "2 P VEICULOS" e o vendedor pesado financiamento
E insiro dados pessoais <cpf> e <nascimento> do cliente pesado financiamento
E insiro <avalista> ou <conjuge> pesado financiamento
E preencho informacoes do <caminhaoproprio> pesado financiamento
E insiro dados do veiculo <placa> e quantidade de <garantia> pesado financiamento
E carrego o resultado parcial pesado financiamento
E valido as informacoes inseridas ate o resultado parcial pesado financiamento
E ajusto o seguro, valor financiado e quantidade de parcelas pesado financiamento
E informo os detalhes e endereco do avalista financiamento pesado financiamento
E insiro os detalhes das informacoes do cliente pesado financiamento
E insiro endereco e observacoes do cliente pesado financiamento
E finalizo a proposta e valido as informacoes financiamento pesado financiamento
Entao acesso o omnifacil e a fila do agente 
E marco o filtro de proposta criada no omni mais
E acesso a proposta, aprovo etapa historica e ficha verificacoes
E acesso a etapa de decisao, envio a proposta para a mesa e preencho o checklist
E acesso a mesa, proposta, aprovo e verifico
E volto a logar no omnimais para emitir o pre contrato
E acesso as fichas aprovadas
E emito o pre contrato
E anexo os documentos e envio

Exemplos:
    |cpf          |nascimento  |placa    |caminhaoproprio|garantia|avalista|conjuge|
    #|"04333332869"|"06/08/1962"|"DTB9C03"|0              |0       |0       |"false"|
    |"00507942809"|"22/12/1959"|"MBI1154"|0              |0       |0       |"false"|

@pesado-liberacao-pagamento
Cenario: Criar nova proposta financiamento pesados - liberacao de pagamento

Quando eu clico em Adicionar ficha pesado financiamento
E seleciono o "Financiamento", "Pesados" e "LOJISTA" pesado financiamento
E seleciono a "2 P VEICULOS" e o vendedor pesado financiamento
E insiro dados pessoais <cpf> e <nascimento> do cliente pesado financiamento
E insiro <avalista> ou <conjuge> pesado financiamento
E preencho informacoes do <caminhaoproprio> pesado financiamento
E insiro dados do veiculo <placa> e quantidade de <garantia> pesado financiamento
E carrego o resultado parcial pesado financiamento
E valido as informacoes inseridas ate o resultado parcial pesado financiamento
E ajusto o seguro, valor financiado e quantidade de parcelas pesado financiamento
E informo os detalhes e endereco do avalista financiamento pesado financiamento
E insiro os detalhes das informacoes do cliente pesado financiamento
E insiro endereco e observacoes do cliente pesado financiamento
E finalizo a proposta e valido as informacoes financiamento pesado financiamento
Entao acesso o omnifacil e a fila do agente 
E marco o filtro de proposta criada no omni mais
E acesso a proposta, aprovo etapa historica e ficha verificacoes
E acesso a etapa de decisao, envio a proposta para a mesa e preencho o checklist
E acesso a mesa, proposta, aprovo e verifico
E volto a logar no omnimais para emitir o pre contrato
E acesso as fichas aprovadas
E emito o pre contrato
E anexo os documentos e envio
Entao acesso o omnifacil e a fila do agente
E marco o filtro de proposta criada no omni mais
E acesso a proposta
E preencho os dados para liberacao do pagamento

Exemplos:
    |cpf          |nascimento  |placa    |caminhaoproprio|garantia|avalista|conjuge|
    #|"04333332869"|"06/08/1962"|"DTB9C03"|0              |0       |0       |"false"|
    |"00507942809"|"22/12/1959"|"MBI1154"|0              |0       |0       |"false"|

@pesado-formalizacao
Cenario: Criar nova proposta financiamento pesados - fluxo completo

Quando eu clico em Adicionar ficha pesado financiamento
E seleciono o "Financiamento", "Pesados" e "LOJISTA" pesado financiamento
E seleciono a "2 P VEICULOS" e o vendedor pesado financiamento
E insiro dados pessoais <cpf> e <nascimento> do cliente pesado financiamento
E insiro <avalista> ou <conjuge> pesado financiamento
E preencho informacoes do <caminhaoproprio> pesado financiamento
E insiro dados do veiculo <placa> e quantidade de <garantia> pesado financiamento
E carrego o resultado parcial pesado financiamento
E valido as informacoes inseridas ate o resultado parcial pesado financiamento
E ajusto o seguro, valor financiado e quantidade de parcelas pesado financiamento
E informo os detalhes e endereco do avalista financiamento pesado financiamento
E insiro os detalhes das informacoes do cliente pesado financiamento
E insiro endereco e observacoes do cliente pesado financiamento
E finalizo a proposta e valido as informacoes financiamento pesado financiamento
Entao acesso o omnifacil e a fila do agente 
E marco o filtro de proposta criada no omni mais
E acesso a proposta, aprovo etapa historica e ficha verificacoes
E acesso a etapa de decisao, envio a proposta para a mesa e preencho o checklist
E acesso a mesa, proposta, aprovo e verifico
E volto a logar no omnimais para emitir o pre contrato
E acesso as fichas aprovadas
E emito o pre contrato
E anexo os documentos e envio
Entao acesso o omnifacil e a fila do agente
E marco o filtro de proposta criada no omni mais
E acesso a proposta
E preencho os dados para liberacao do pagamento

Exemplos:
    |cpf          |nascimento  |placa    |caminhaoproprio|garantia|avalista|conjuge|
    #|"04333332869"|"06/08/1962"|"DTB9C03"|0              |0       |0       |"false"|
    |"00507942809"|"22/12/1959"|"MBI1154"|0              |0       |0       |"false"|

