                  #language:pt
                  @pesados_brasil
                  @190
                  Funcionalidade: Acessar o menu pesados brasil

                  Contexto:
                  Dado que eu esteja logado no omni mais
                  Quando clico na opcao carteira

                  @validaTexto
                  Cenario: Validar texto
                  Quando eu valido se o texto esta conforme o esperado

                  @cepInvalido
                  Cenario: Cep invalido
                  Quando pesquiso um cep invalido

                  @cepValido
                  Cenario: Cep valido
                  Quando pesquiso um cep valido

                  @cepSemAgente
                  Cenario: Cep sem agente
                  Quando pesquiso um cep que nao tem agente perto

                  @propostaCep
                  Cenario: Preenchimento da proposta
                  Quando eu preencho a proposta valido o cep


                  @dados
                  Cenario: Endereco
                  Quando insiro dado pessoais <cpf> e <nascimento> do cliente pesado financiamento
                  E insiro <avalista> ou <conjuge> pesados financiamento
                  E preencho informacoes do <caminhaoproprio> pesados financiamento
                  E insiro dados do veiculos <placa> e quantidade de <garantia> pesado financiamento
                  #E carrego o resultado parcial pesados financiamento
                  E insiro os detalhes das informacoes do cliente pesados financiamento
                  E insiro endereco e observacoes do cliente pesados financiamento


                  Exemplos:
                  | cpf           | nascimento   | placa     | caminhaoproprio | garantia | avalista | conjuge |
                  | "04333332869" | "06/08/1962" | "DTB9C03" | 0               | 0        | 0        | "false" |
                  ##  |"00507942809"|"22/12/1959"|"MBI1154"|0              |0       |0       |"false"|


                  @verificaCepRegistrado
                  Cenario: Verifica se o cep se mantem o mesmo do que foi digitado na primeira tela
                  Dado que eu insira um cep valido para consulta
                  E clico em consultar cep
                  Entao devo ver o botao para cadastrar ficha pesados brasil
                  Quando clico no botao para cadastrar a ficha pesados brasil
                  E seleciono o "Financiamento"
                  E seleciono o nome do vendedor
                  Entao devo ver o mesmo cep que foi digitado anteriormente

                  @verifica-popup-confirmacao-proposta @323
                  Cenario: Verificar popup de confirmacao de envio de proposta Pesados Brasil

                  Dado que eu insira um cep valido para consulta
                  E clico em consultar cep
                  Entao devo ver o botao para cadastrar ficha pesados brasil
                  Quando clico no botao para cadastrar a ficha pesados brasil
                  E seleciono o "Financiamento"
                  E seleciono o nome do vendedor
                  E insiro dados pessoais <cpf> e <nascimento> do cliente com extrato
                  E seleciono primeiro caminhao
                  E seleciono veiculo 0km
                  E seleciono as informacoes do veiculo <categoria>,<marca>,<ano>,<modelo>,<versao>,<cor>,<combustivel>,<estado>
                  E carrego o resultado parcial
                  E insiro mais informacoes do cliente de pesados
                  E insiro endereco do cliente
                  E preencho observacoes
                  Entao devo ver o popup para confirmacao de envio para o agente de pesados

                  Exemplos:
                  | cpf           | nascimento   | categoria  | marca   | ano    | modelo   | versao                 | cor     | combustivel | estado           |
                  | "16601794830" | "06/07/1971" | "CAMINHAO" | "VOLVO" | "2020" | "FH-420" | "4X2 2P (DIESEL) (E5)" | "PRETA" | "ALCOOL"    | "SP - São Paulo" |

                  @verifica-cancela-envio-proposta @323
                  Cenario: Verificar popup de confirmacao de envio de proposta Pesados Brasil

                  Dado que eu insira um cep valido para consulta
                  E clico em consultar cep
                  Entao devo ver o botao para cadastrar ficha pesados brasil
                  Quando clico no botao para cadastrar a ficha pesados brasil
                  E seleciono o "Financiamento"
                  E seleciono o nome do vendedor
                  E insiro dados pessoais <cpf> e <nascimento> do cliente com extrato
                  E seleciono primeiro caminhao
                  E seleciono veiculo 0km
                  E seleciono as informacoes do veiculo <categoria>,<marca>,<ano>,<modelo>,<versao>,<cor>,<combustivel>,<estado>
                  E carrego o resultado parcial
                  E insiro mais informacoes do cliente de pesados
                  E insiro endereco do cliente
                  E preencho observacoes
                  E clico para cancelar o envio da proposta para o agente de pesados
                  Entao devo ver a tela de observacoes

                  Exemplos:
                  | cpf           | nascimento   | categoria  | marca   | ano    | modelo   | versao                 | cor     | combustivel | estado           |
                  | "16601794830" | "06/07/1971" | "CAMINHAO" | "VOLVO" | "2020" | "FH-420" | "4X2 2P (DIESEL) (E5)" | "PRETA" | "ALCOOL"    | "SP - São Paulo" |
