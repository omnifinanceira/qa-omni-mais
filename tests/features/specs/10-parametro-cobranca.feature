#language:pt
@10 @parametro-cobranca
Funcionalidade: Parâmetros de Cobrança


@validar-usuario-com-permissao
Cenario: Validação do acesso ao Parâmetros de Cobrança ao usuário com permissão
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
Entao a pagina parametrôs de cobrança é aberta


@validar-usuario-sem-permissao
Cenario: Validação do acesso ao Parâmetros de Cobrança ao usuário sem permissão
Dado que acesso o omnifacil com usuário sem permissão ao parametro de cobrança
Quando acesso o novo menu
Então o sub menu Canais Digitais não é apresentado

@validar-filtro-campanha
Cenario: Validação do filtro Campanha
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E eu selecionar no filtro Campanha Pesados Brasil
Então deve listar somente os parâmetros referente a campanha Pesados Brasil

@validar-filtro-distribuicao
Cenario: Validação do filtro Distribuição	
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E seleciono o filtro Distripuição por Proximidade
Então devera listar somente os parâmetros referente a distribuição Proximidade

@validar-filtro-situacao
Cenario: Validação do filtro Situação
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E eu seleciono o filtro Situação pelo checkbox Inativo
Então deve lista somente os parâmetros Inativos

@validar-filtro-vigencia
Cenario: Validação do filtro Vigência
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E informar o início da vigência uma data maior que o fim da vigência
Então não deve realizar a pesquisa

@edicao-campanha
Cenario: Validação da edição da Campanha do parâmetro
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E eu efetuo alguma edição de Campanha
Então a Campanha deve ter sido alterada

@edicao-distribuicao-parametro
Cenario: Validação da edição da Distribuição da Cobrança do parâmetro
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E eu efetuo alguma edição de Distribuição de parâmetro
Então a Distribuição de parâmetro deve ter sido alterada

@validar-botao-voltar
Cenario: Validação do botão Voltar na edição
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E eu acesso a pagina de edição
E e clico no botão voltar 
Então a pagina parametrôs de cobrança é aberta


@cadastro-excluir-truncadao
Cenario: Cadastro/Exclusão de um parâmetro de cobrança de uma Campanha Trucadão
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E adiciono uma nova campanha truncadão
Então novo parametro é criado
Quando excluo um parametro de cobrança truncadão
Então o parametro devera ser excluido

@cadastro-excluir-leads
Cenario: Cadastro de um parâmetro de cobrança de uma Campanha Leads
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E adiciono uma nova campanha lead
Então novo parametro é criado
Quando excluo um parametro de cobrança lead
Então o parametro devera ser excluido

@cadastro-excluir-pesados-brasil
Cenario: Cadastro de um parâmetro de cobrança de uma Campanha Pesados Brasil
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E adiciono uma nova campanha Pesados Brasil
Então novo parametro é criado
Quando excluo um parametro de cobrança Pesados Brasil
Então o parametro devera ser excluido

@cancelar-exclusao
Cenario: Validação do botão Cancelar na exclusão
Dado que acesso o omnifacil com usuário de parametro de cobrança
Quando acesso os parametrôs de cobrança
E clico no botão cancelar da exclusão
Então o parametro não deve ser excluido


