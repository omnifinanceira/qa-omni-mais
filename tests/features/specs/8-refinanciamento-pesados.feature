#language:pt
@8 @refinanciamento-pesados
Funcionalidade: Criar nova proposta refinanciamento pesados

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta refinanciamento pesados

Quando eu clico em Adicionar ficha pesado refinanciamento
E seleciono o "Refinanciamento", "Pesados" e "LOJISTA" pesado refinanciamento
E seleciono a "2 P VEICULOS" e o vendedor pesado refinanciamento
E insiro dados pessoais <cpf> e <nascimento> do cliente pesado refinanciamento
E insiro <avalista> ou <conjuge> pesado refinanciamento
E preencho informacoes do <caminhaoproprio> pesado refinanciamento
E insiro dados do veiculo <placa> e quantidade de <garantia> pesado refinanciamento
E carrego o resultado parcial pesado refinanciamento
E valido as informacoes inseridas ate o resultado parcial pesado refinanciamento
E ajusto o seguro, valor financiado e quantidade de parcelas pesado refinanciamento
E informo os detalhes e endereco do avalista refinanciamento pesado refinanciamento
E informo os detalhes e endereco do avalista refinanciamento pesado refinanciamento
E insiro os detalhes das informacoes do cliente pesado refinanciamento
E insiro endereco e observacoes do cliente pesado refinanciamento
E finalizo a proposta e valido as informacoes refinanciamento pesado refinanciamento

Exemplos:
    |cpf          |nascimento  |placa    |caminhaoproprio|garantia|avalista|conjuge|
    |"00507942809"|"22/12/1959"|"MBI1154"|0              |0       |0       |"false"|
