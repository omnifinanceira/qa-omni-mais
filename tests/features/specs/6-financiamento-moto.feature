            #language:pt
            @6 @financiamento-motos
            Funcionalidade: Criar nova proposta financiamento motos

            Contexto:
            Dado que eu esteja logado como lojista

            Cenario: Criar nova proposta financiamento motos
            Quando eu clico em Adicionar ficha motos
            E seleciono o tipo de operacao financiamento para a ficha motos
            E seleciono o produto motos
            #E seleciono o solicitante para a ficha motos
            E seleciono o vendedor motos
            E insiro dados pessoais <cpf> e <nascimento> do cliente motos
            E insiro dados do veiculo <placa> motos
            E carrego o resultado parcial motos
            E ajusto o valor financiado e quantidade de parcelas motos
            E insiro mais informacoes do cliente motos
            E insiro endereco do cliente motos
            E preencho observacoes motos
            Entao valido se a ficha foi criada e enviada para analise motos
            E validar os dados da proposta motos

            Exemplos:
            | cpf           | nascimento   | placa     |
            | "16601794830" | "06/07/1971" | "DXY3650" |