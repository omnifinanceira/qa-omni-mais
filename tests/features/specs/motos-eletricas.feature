            #language: pt

            @motos-eletricas
            Funcionalidade: Motos eletricas no OmniMais

            Contexto:
            Dado que eu esteja logado como lojista

            @motos-eletricas-tipo-combustivel
            Cenario: Lançamento de uma proposta de uma moto elétrica

            Quando eu clico em Adicionar ficha
            E seleciono o "Financiamento", "Motocicletas" e "LOJISTA"
            E seleciono a loja "2 P VEICULOS" e o vendedor
            E insiro dados pessoais <cpf> e <nascimento> do cliente
            E seleciono veiculo 0km
            E seleciono as informacoes do veiculo <categoria>,<marca>,<ano>,<modelo>,<versao>,<cor>,<combustivel>,<estado>
            E carrego o resultado parcial
            E insiro mais informacoes do cliente
            E insiro endereco do cliente
            E preencho observacoes
            Então valido se a ficha foi criada e enviada para analise

            Exemplos:
            | cpf           | nascimento   | categoria     | marca   | ano    | modelo | versao            | cor     | combustivel | estado           |
            | "38056144897" | "02/10/1994" | "MOTOCICLETA" | "VOLTZ" | "2021" | "EV01" | "1800W(ELETRICA)" | "PRETA" | "ELÉTRICO"  | "SP - São Paulo" |
            #| "16601794830" | "06/07/1971" | "MOTOCICLETA" | "VOLTZ" | "2021" | "EV01" | "1800W(ELETRICA)" | "PRETA" | "ELÉTRICO"  | "SP - São Paulo" |
            #| "16601794830" | "06/07/1971" | "MOTOCICLETA" | "HONDA" | 2021 | "BIZ"  | "110I"            | "AZUL"  | "BI-COMBUSTÍVEL" | "SP - São Paulo" |

            @opcao-eletrica
            Cenario: Validar se foi incluída a opção ‘Elétrica’ em Tipo de Combustível
            Quando eu clico em Adicionar ficha
            E seleciono o "Financiamento", "Motocicletas" e "LOJISTA"
            E seleciono a loja "2 P VEICULOS" e o vendedor
            E insiro dados pessoais <cpf> e <nascimento> do cliente
            Quando seleciono veículo 0km
            Então deve listar a opção ‘Elétrico’ em Tipo de Combustível.
            Exemplos:
            | cpf           | nascimento   | categoria     | marca   | ano  | modelo | versao            | cor     | combustivel | estado           |
            | "16601794830" | "06/07/1971" | "MOTOCICLETA" | "VOLTZ" | 2021 | "EV01" | "1800W(ELETRICA)" | "PRETA" | "ELÉTRICO"  | "SP - São Paulo" |

            @motos-eletricas-resultado-parcial
            Cenario: Garantir o fluxo do preenchimento da proposta de uma moto elétrica (após a criação do código Fipe Fake).

            Quando eu clico em Adicionar Ficha
            E seleciono o "Financiamento", "Motocicletas" e "LOJISTA"
            E seleciono a loja "2 P VEICULOS" e o vendedor
            E insiro dados pessoais <cpf> e <nascimento> do cliente
            E seleciono veiculo 0km
            E seleciono as informacoes do veiculo <categoria>,<marca>,<ano>,<modelo>,<versao>,<cor>,<combustivel>,<estado>
            E carrego o resultado parcial

            Exemplos:
            | cpf           | nascimento   | categoria     | marca   | ano  | modelo | versao            | cor     | combustivel | estado           |
            | "16601794830" | "06/07/1971" | "MOTOCICLETA" | "VOLTZ" | 2021 | "EV01" | "1800W(ELETRICA)" | "PRETA" | "ELÉTRICO"  | "SP - São Paulo" |

            @motos-eletricas-comissao
            Cenario: Verificacao da comissao mudando de 0 para 5

            Quando eu clico em Adicionar Ficha
            E seleciono o "Financiamento", "Motocicletas" e "LOJISTA"
            E seleciono a loja "2 P VEICULOS" e o vendedor
            E insiro dados pessoais <cpf> e <nascimento> do cliente
            E seleciono veiculo 0km
            E seleciono as informacoes do veiculo <categoria>,<marca>,<ano>,<modelo>,<versao>,<cor>,<combustivel>,<estado>
            E clico para alterar a comissao da proposta
            E altero a comissao da proposta
            E verifico se o valor mudou apos alterar a comissao

            Exemplos:
            | cpf           | nascimento   | categoria     | marca   | ano    | modelo | versao            | cor     | combustivel | estado           |
            | "16601794830" | "06/07/1971" | "MOTOCICLETA" | "VOLTZ" | "2021" | "EV01" | "1800W(ELETRICA)" | "PRETA" | "ELÉTRICO"  | "SP - São Paulo" |

            @comissao-refinanciamento
            Cenario: Verificacao da comissao em refinanciamento para nao alterar de 0

            Quando eu clico em Adicionar Ficha
            E seleciono o "Refinanciamento", "Motocicletas" e "LOJISTA"
            E seleciono a loja "2 P VEICULOS" e o vendedor
            E insiro dados pessoais <cpf> e <nascimento> do cliente
            E seleciono veiculo 0km
            E seleciono as informacoes do veiculo <categoria>,<marca>,<ano>,<modelo>,<versao>,<cor>,<combustivel>,<estado>
            E clico para alterar a comissao da proposta
            E altero a comissao da proposta
            E verifico que a comissao se manteve

            Exemplos:
            | cpf           | nascimento   | categoria     | marca   | ano    | modelo | versao            | cor     | combustivel | estado           |
            | "16601794830" | "06/07/1971" | "MOTOCICLETA" | "VOLTZ" | "2021" | "EV01" | "1800W(ELETRICA)" | "PRETA" | "ELÉTRICO"  | "SP - São Paulo" |


            @seguro-indisponivel-para-71-anos-ou-mais
            Cenario: Verificacao da indisponibilidade de seguro para pessoas com 71 anos ou mais

            Quando eu clico em Adicionar Ficha
            E seleciono o "Financiamento", "Motocicletas" e "LOJISTA"
            E seleciono a loja "2 P VEICULOS" e o Nome do Vendedor
            E insiro dados pessoais <cpf> e <nascimentoMaior> do cliente
            E seleciono veículo 0km
            E seleciono a <categoria> do veículo
            E seleciono a <marca> do veículo
            E seleciono o <ano> do veículo
            E seleciono o <modelo> do veículo
            E seleciono a <versao> do veiculo
            E seleciono a <cor> do veiculo
            E seleciono o <combustivel> do veiculo
            E seleciono o <estado> de Licenciamento do veículo
            E clico para alterar a comissao da proposta
            E verifico se o seguro esta disponivel

            Exemplos:
            | cpf           | nascimento   | categoria     | marca   | ano    | modelo | versao            | cor     | combustivel | estado           | nascimentoMaior |
            | "16601794830" | "06/07/1971" | "MOTOCICLETA" | "VOLTZ" | "2021" | "EV01" | "1800W(ELETRICA)" | "PRETA" | "ELÉTRICO"  | "SP - São Paulo" | "20/05/1949"    |


            @verifica-aprova-proposta-com-seguro
            Cenario: Verifica a aprovacao de proposta com seguro

            Quando eu clico em Adicionar Ficha
            E seleciono o "Financiamento", "Motocicletas" e "LOJISTA"
            E seleciono a loja "2 P VEICULOS" e o Nome do Vendedor
            E insiro dados pessoais <cpf> e <nascimentoMaior> do cliente
            E seleciono veículo 0km
            E seleciono a <categoria> do veículo
            E seleciono a <marca> do veículo
            E seleciono o <ano> do veículo
            E seleciono o <modelo> do veículo
            E seleciono a <versao> do veiculo
            E seleciono a <cor> do veiculo
            E seleciono o <combustivel> do veiculo
            E seleciono o <estado> de Licenciamento do veículo
            E carrego o resultado parcial
            E insiro mais informacoes do cliente
            E insiro endereco do cliente
            E preencho observacoes
            E valido se a ficha foi criada e enviada para analise
            Entao acesso o omnifacil e a fila do agente para verificar o seguro
            E marco o filtro de proposta criada no omni mais
            E acesso a proposta para aprovacao
            E seleciono o menu Ficha/Verificações
            E clico no botao de seguros
            E valido se tem seguros disponiveis

            Exemplos:
            | cpf           | nascimento   | categoria     | marca   | ano    | modelo | versao            | cor     | combustivel | estado           | nascimentoMaior |
            | "16601794830" | "06/07/1971" | "MOTOCICLETA" | "VOLTZ" | "2021" | "EV01" | "1800W(ELETRICA)" | "PRETA" | "ELÉTRICO"  | "SP - São Paulo" | "20/05/1949"    |