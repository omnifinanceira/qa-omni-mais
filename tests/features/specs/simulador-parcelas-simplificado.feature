#language:pt
@simulador-parcelas
Funcionalidade: Simulador de parcela simplificado

Contexto:
    Dado que eu esteja logado como lojista

@financiamento-leve-lojista
Cenario: Criar nova simulacao financiamento leve lojista

Quando eu clico em simulador
E seleciono o "Financiamento", "Automóveis" e "LOJISTA"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2015" |"5000000"      |"2000000"          |

@financiamento-leve-balcao
Cenario: Criar nova simulacao financiamento leve balcao

Quando eu clico em simulador
E seleciono o "Financiamento", "Automóveis" e "CLIENTE NO BALCÃO"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2017" |"6000000"      |"3000000"          |

@financiamento-moto-lojista
Cenario: Criar nova simulacao financiamento moto lojista

Quando eu clico em simulador
E seleciono o "Financiamento", "Motocicletas" e "LOJISTA"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2018" |"2000000"      |"1000000"          |

@financiamento-moto-balcao
Cenario: Criar nova simulacao financiamento moto balcao

Quando eu clico em simulador
E seleciono o "Financiamento", "Motocicletas" e "CLIENTE NO BALCÃO"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2014" |"2500000"      |"1500000"          |

@financiamento-pesado-lojista
Cenario: Criar nova simulacao financiamento pesado lojista

Quando eu clico em simulador
E seleciono o "Financiamento", "Pesados" e "LOJISTA"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2013" |"8000000"      |"5000000"          |

@financiamento-pesado-balcao
Cenario: Criar nova simulacao financiamento pesado balcao

Quando eu clico em simulador
E seleciono o "Financiamento", "Pesados" e "CLIENTE NO BALCÃO"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2012" |"9000000"      |"6000000"          |

@refinanciamento-leve-lojista
Cenario: Criar nova simulacao refinanciamento leve lojista

Quando eu clico em simulador
E seleciono o "Refinanciamento", "Automóveis" e "LOJISTA"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2015" |"5000000"      |"2000000"          |

@refinanciamento-leve-balcao
Cenario: Criar nova simulacao refinanciamento leve balcao

Quando eu clico em simulador
E seleciono o "Refinanciamento", "Automóveis" e "CLIENTE NO BALCÃO"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2017" |"6000000"      |"3000000"          |

@refinanciamento-moto-lojista
Cenario: Criar nova simulacao refinanciamento moto lojista

Quando eu clico em simulador
E seleciono o "Refinanciamento", "Motocicletas" e "LOJISTA"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2018" |"2000000"      |"1000000"          |

@refinanciamento-moto-balcao
Cenario: Criar nova simulacao refinanciamento moto balcao

Quando eu clico em simulador
E seleciono o "Refinanciamento", "Motocicletas" e "CLIENTE NO BALCÃO"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2014" |"2500000"      |"1500000"          |

@refinanciamento-pesado-lojista
Cenario: Criar nova simulacao refinanciamento pesado lojista

Quando eu clico em simulador
E seleciono o "Refinanciamento", "Pesados" e "LOJISTA"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2013" |"8000000"      |"5000000"          |

@refinanciamento-pesado-balcao
Cenario: Criar nova simulacao refinanciamento pesado balcao

Quando eu clico em simulador
E seleciono o "Refinanciamento", "Pesados" e "CLIENTE NO BALCÃO"
E seleciono a loja "2 P VEICULOS"
E seleciono o <ano> modelo
E insiro o valor do veículo <valor_veiculo>
E insiro o valor do financiado <valor_financiado>
Então carrego a simulação

Exemplos:
    |ano    |valor_veiculo  |valor_financiado   |
    |"2012" |"9000000"      |"6000000"          |