#language:pt
@efetive_mais
@567
Funcionalidade: Efetive

Contexto: 
    # Dado que eu esteja logado no omni facil 
    # Quando clico no novo menu
    # E seleciono a aba operacional 
    # E carrego a pagina

@preenche_filtro_efetive
#Moto - 2015 - SP - SÃO PAULO - 10% ENTRADA - 50% COMP. RENDA 
Cenario: filtrar produto moto 2015 sp 
Quando preencho o filtro de pesquisa com as informacoes <produto>, <ano>, <estado>, <cidade>, <entrada>, <compRenda>
Exemplos:
|    produto             |  ano   | estado        |   cidade     | entrada | compRenda |
|    "Moto"              | "2015" |  "SP"       |  "SAO PAULO"   | "até 25%"| "até 10%"|
|    "Moto"              | "2019" |  "SP"       |  "SAO PAULO"   | "até 25%"| "até 10%"|
|    "Veículo Leve"      | "2007" |  "SP"       |  "SAO PAULO"   | "até 30%"| "até 25%"|
|    "Veículo Leve"      | "2007" |  "SP"       |  "SAO PAULO"   | "até 75%"| "até 75%"|
|    "Veículo Pesado"    | "2008" |  "SP"       |  "SAO PAULO"   | "até 50%"| "até 30%"|

Cenario: filtrar produto todos
Quando seleciono produto todos

Cenario: botao limpar
Quando clico no botao limpar

@pesquisaLetra
Cenario: botao pesquisar letra
Quando clico no botao pesquisar letra

@pesquisaNumero
Cenario: botao pesquisar numero
Quando clico no botao pesquisar numero

@valorParaSerFinanciado
Cenario: Valor a ser financiado
Quando coloco o valor a ser financiado

@verificaScore
Cenario: Verificar Score
Quando verifico o score 

@mesmoAgente
Cenario: Mesmo agente
Quando clico no mesmo agente

# @317
# Cenario: Logando com usuario 317MARIA
# Dado que eu esteja no omni facil 
# Quando clico no botao novo menu

@Agente 
Cenario: Capturar proposta de agente diferente
Quando capturo uma proposta de agente diferente

@capturaPropostaSucesso
Cenario: Captura de uma proposta aprovada usando a api
Quando teste

@capturaPropostaSucesso
Cenario: Validacao que a proposta nao aparece no efetive+ sem utilizar a api
Quando teste

@validaPropostaPreContrato
Cenario: Validacao que a proposta nao aparece no efetive+ quando esta no status de emissao de pre-contrato
Dado que eu esteja logado no OmniFacil para acessar a mesa de credito
E seleciono a mesa de credito
E seleciono a mesa de veiculos leves
E aprovo a proposta enviada para a mesa