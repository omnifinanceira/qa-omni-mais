            #language:pt
            @4 @financiamento-leve
            Funcionalidade: Criar nova proposta financiamento leve

            Contexto:
            Dado que eu esteja logado como lojista

            @leve-finalizar-proposta
            Cenario: Criar nova proposta financiamento leve

            Quando eu clico em Adicionar ficha leve financiamento
            E seleciono o "Financiamento", "Automóveis" e "LOJISTA" leve financiamento
            E seleciono a "2 P VEICULOS" e o vendedor leve financiamento
            E insiro dados pessoais <cpf> e <nascimento> do cliente leve financiamento
            #E insiro <avalista> ou <conjuge> pesado financiamento
            E insiro dados do veiculo <placa> leve financiamento
            E carrego o resultado parcial leve financiamento
            E ajusto o valor financiado e quantidade de parcelas leve financiamento
            E ajusto as preferencias do <seguro_prestamista> e da <garantia_mecanica> leve financiamento
            # E informo os detalhes e endereco do avalista financiamento pesado financiamento
            E valido as alteracoes e capturo os valores
            E insiro mais informacoes do cliente leve financiamento
            E insiro endereco do cliente leve financiamento
            E preencho observacoes leve financiamento
            E valido se a ficha foi criada e enviada para analise leve

            Exemplos:
            | cpf           | nascimento   | placa     | garantia | avalista | conjuge | seguro_prestamista | garantia_mecanica |
            | "16601794830" | "06/07/1971" | "FHC9383" | 0        | 0        | "false" | 

            @leve-mesa
            Cenario: Criar nova proposta e aprovar na mesa

            Quando eu clico em Adicionar ficha leve financiamento
            E seleciono o "Financiamento", "Automóveis" e "LOJISTA" leve financiamento
            E seleciono a "2 P VEICULOS" e o vendedor leve financiamento
            E insiro dados pessoais <cpf> e <nascimento> do cliente leve financiamento
            #E insiro <avalista> ou <conjuge> pesado financiamento
            E insiro dados do veiculo <placa> leve financiamento
            E carrego o resultado parcial
            #E ajusto o valor financiado e quantidade de parcelas leve financiamento
            # E ajusto as preferencias do seguro leve financiamento
            # E informo os detalhes e endereco do avalista financiamento pesado financiamento
            #E valido as alteracoes e capturo os valores
            E insiro mais informacoes do cliente
            E insiro endereco do cliente old
            E preencho observacoes
            E valido se a ficha foi criada e enviada para analise
            Entao acesso o omnifacil e a fila do agente
            E marco o filtro de proposta criada no omni mais
            E acesso a proposta, aprovo etapa historica e ficha verificacoes
            E acesso a etapa de decisao, envio a proposta para a mesa e preencho o checklist
            E acesso a mesa, proposta, aprovo e verifico

            Exemplos:
            | cpf           | nascimento   | placa     | garantia | avalista | conjuge |
            | "16601794830" | "06/07/1971" | "FHC9383" | 0        | 0        | "false" |

            @leve-mesa-alterado
            Cenario: Criar nova proposta e aprovar na mesa

            Quando eu clico em Adicionar ficha leve financiamento
            E seleciono o "Financiamento", "Automóveis" e "LOJISTA" leve financiamento
            E seleciono a "2 P VEICULOS" e o vendedor leve financiamento
            E insiro dados pessoais <cpf> e <nascimento> do cliente leve financiamento
            #E insiro <avalista> ou <conjuge> pesado financiamento
            E insiro dados do veiculo <placa> leve financiamento
            E carrego o resultado parcial leve financiamento
            E ajusto o valor financiado e quantidade de parcelas leve financiamento alterado
            # E ajusto as preferencias do seguro leve financiamento
            # E informo os detalhes e endereco do avalista financiamento pesado financiamento
            E valido as alteracoes e capturo os valores
            E insiro mais informacoes do cliente leve financiamento
            E insiro endereco do cliente leve financiamento
            E preencho observacoes leve financiamento
            E valido se a ficha foi criada e enviada para analise leve
            Entao acesso o omnifacil e a fila do agente para alterar o valor liberado
            E marco o filtro de proposta criada no omni mais
            E acesso a proposta, aprovo etapa historica e ficha verificacoes e altero o valor liberado
            E acesso a etapa de decisao, envio a proposta para a mesa e preencho o checklist
            E acesso a mesa, proposta, aprovo e verifico

            Exemplos:
            | cpf           | nascimento   | placa     | garantia | avalista | conjuge |
            | "04333332869" | "06/08/1962" | "DTV7371" | 0        | 0        | "false" |