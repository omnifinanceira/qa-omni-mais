#language:pt
@7 @refinanciamento-leve
Funcionalidade: Criar nova proposta refinanciamento leve

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta refinanciamento leve
Quando eu clico em Adicionar ficha
E seleciono o "Refinanciamento", "Automóveis" e "LOJISTA"
E seleciono a loja "2 P VEICULOS" e o vendedor
E insiro dados pessoais <cpf> e <nascimento> do cliente
E insiro dados do veiculo leve <placa>
E carrego o resultado parcial
E ajusto o valor financiado e quantidade de parcelas leve refi
E insiro mais informacoes do cliente
E insiro endereco do cliente
E preencho observacoes
Entao valido se a ficha foi criada e enviada para analise
E validar os dados da proposta leve refi


Exemplos:
    |cpf          |nascimento  |placa    |
    |"16601794830"|"06/07/1971"|"FHC9383"|